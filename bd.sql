-- ALTER USER 'root'@'localhost' IDENTIFIED BY 'p';

-- CREATE DATABASE scripted;
-- USE scripted;

-- DROP TABLE Contenir;
-- DROP TABLE Participer;
-- DROP TABLE Resoudre;
-- DROP TABLE Enigme;
-- DROP TABLE Partie;
-- DROP TABLE Utilisateur;

CREATE TABLE  Utilisateur(
email varchar(50) PRIMARY KEY,
pseudo varchar(50),
mdp varchar(50),
estAdmin boolean
);

CREATE TABLE  Enigme(
id integer PRIMARY KEY AUTOINCREMENT,
nom varchar(50),
enonce varchar(250) NOT NULL,
aide varchar(250),
rappel varchar(250),
exemple varchar(250),
solution varchar(250) NOT NULL,
test varchar(250) NOT NULL,
ordre integer CHECK (ordre >=0),
tempsDeResolution numeric CHECK (tempsDeResolution >=0),
points numeric CHECK (points >=0),
prompt varchar(250)
);

CREATE TABLE  Partie(
id integer PRIMARY KEY AUTOINCREMENT,
dateDebut date NOT NULL
);

CREATE TABLE  Resoudre(
utilisateur varchar(50) REFERENCES Utilisateur(email),
enigme int REFERENCES Enigme(id),
partie int REFERENCES Partie(id),
classement int CHECK (classement >0),
indexEnigme int CHECK (indexEnigme >0),
temps numeric CHECK (temps >0),
code varchar(500),
ended boolean,
enMulti boolean,
PRIMARY KEY(utilisateur, enigme, partie)
);

CREATE TABLE  Contenir(
partie int REFERENCES Partie(id),
enigme int REFERENCES Enigme(id),
indexEnigme numeric ,
PRIMARY KEY(partie, enigme)
);

CREATE TABLE  Participer(
    partie int REFERENCES Partie(id),
    utilisateur varchar(50) REFERENCES Utilisateur(email),
    etat int CHECK (etat IN (0,1,2)), -- etat 0 = enAttente etat 1 = enCours etat 2 = fini
    PRIMARY KEY(partie, utilisateur)
);