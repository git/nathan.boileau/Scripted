<?php

include_once "Enigme.php";

class Partie
{
    private int $idPartie;
    private DateTime $datePartie;
    private array $listeEnigme;

    /**
     * @param int $idPartie
     * @param array $datePartie
     */
    public function __construct(int $idPartie = 0, array $listeEnigme)
    {
        $this->idPartie = $idPartie;
        $this->datePartie = new DateTime();
        $this->listeEnigme = $listeEnigme;
    }

    /**
     * @return int
     */
    public function getIdPartie(): int
    {
        return $this->idPartie;
    }

    /**
     * @param int $idPartie
     */
    public function setIdPartie(int $idPartie): void
    {
        $this->idPartie = $idPartie;
    }

    /**
     * @return array
     */
    public function getDatePartie(): dateTime
    {
        return $this->datePartie;
    }

    /**
     * @param dateTime $datePartie
     */
    public function setDatePartie(dateTime $datePartie): void
    {
        $this->datePartie = $datePartie;
    }

    /**
     * @param array $listeEnigme
     */

    public function getListeEnigme(): array
    {
        return $this->listeEnigme;
    }
    public function setListeEnigme(array $listeEnigme): void
    {
        $this->listeEnigme = $listeEnigme;
    }
}