<?php

class Enigme
{
    private int $idEnigme;
    private string $nom;
    private string $enonce;
    private string $aide;
    private string $rappel;
    private string $exemple;
    private string $solution;
    private string $test;
    private int $ordre;
    private int $tempsDeResolution;
    private int $points;
    private string $prompt;

    /**
     * @param int $idEnigme
     * @param string $nom
     * @param string $enonce
     * @param string $aide
     * @param string $rappel
     * @param string $exemple
     * @param string $solution
     * @param string $test
     * @param int $ordre
     * @param int $tempsDeResolution
     * @param int $points
     * @param string $prompt
     */
    public function __construct($idEnigme, $nom = "",$enonce = "", $aide = "", $rappel = "", 
    $exemple = "",$solution = "", $test = "", $ordre = -1, $tempsDeResolution = 0, $points = 0,
    $prompt = "")
    {
        $this->idEnigme = $idEnigme;
        $this->nom = $nom;
        $this->enonce = $enonce;
        $this->aide = $aide;
        $this->rappel = $rappel;
        $this->exemple = $exemple;
        $this->solution = $solution;
        $this->test = $test;
        $this->ordre = $ordre;
        $this->tempsDeResolution = $tempsDeResolution;
        $this->points = $points;
        $this->prompt = $prompt;
    }
    /**
     * @return int
     */
    public function getIdEnigme(): int
    {
        return $this->idEnigme;
    }

    /**
     * @param int $idEnigme
     */
    public function setIdEnigme(int $idEnigme): void
    {
        $this->idEnigme = $idEnigme;
    }

    /**
     * @return string
     */
    public function getNom(): string
    {
        return $this->nom;
    }

    /**
     * @param string $nom
     */
    public function setNom(string $nom): void
    {
        $this->nom = $nom;
    }

    /**
     * @return string
     */
    public function getEnonce(): string
    {
        return $this->enonce;
    }

    /**
     * @param string $enonce
     */
    public function setEnonce(string $enonce): void
    {
        $this->enonce = $enonce;
    }

    /**
     * @return string
     */
    public function getAide(): string
    {
        return $this->aide;
    }

    /**
     * @param string $aide
     */
    public function setAide(string $aide): void
    {
        $this->aide = $aide;
    }

    /**
     * @return string
     */
    public function getRappel(): string
    {
        return $this->rappel;
    }

    /**
     * @param string $rappel
     */
    public function setRappel(string $rappel): void
    {
        $this->rappel = $rappel;
    }

     /**
     * @return string
     */
    public function getExemple(): string
    {
        return $this->exemple;
    }

    /**
     * @param string $exemple
     */
    public function setExemple(string $exemple): void
    {
        $this->exemple = $exemple;
    }

    /**
     * @return string
     */
    public function getSolution(): string
    {
        return $this->solution;
    }

    /**
     * @param string $solution
     */
    public function setSolution(string $solution): void
    {
        $this->solution = $solution;
    }

    /**
     * @return string
     */
    public function getTest(): string
    {
        return $this->test;
    }

    /**
     * @param string $test
     */
    public function setTest(string $test): void
    {
        $this->test = $test;
    }
    public function getOrdre(): int
    {
        return $this->ordre;
    }
    public function setOrdre(int $ordre): void
    {
        $this->ordre = $ordre;
    }

    /**
     * @return int
     */
    public function getTempsDeResolution(): int
    {
        return $this->tempsDeResolution;
    }

    /**
     * @param int $tempsDeResolution
     */
    public function setTempsDeResolution(int $tempsDeResolution): void
    {
        $this->tempsDeResolution = $tempsDeResolution;
    }
    public function getPoints(): int
    {
        return $this->points;
    }
    public function setPoints(int $points): void
    {
        $this->points = $points;
    }

    public function getPrompt(): string
    {
        return $this->prompt;
    }
    public function setPrompt(string $prompt): void
    {
        $this->prompt = $prompt;
    }
}