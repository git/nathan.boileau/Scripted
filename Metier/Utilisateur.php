<?php

class Utilisateur
{
    private string $email;
    private string $pseudo;
    private string $mdp;
    private bool $estAdmin;

    /**
     * @param string $email
     * @param string $pseudo
     * @param string $mdp
     * @param bool $estAdmin
     */
    public function __construct(string $email, string $pseudo, string $mdp, bool $estAdmin)
    {
        $this->email = $email;
        $this->pseudo = $pseudo;
        $this->mdp = $mdp;
        $this->estAdmin = $estAdmin;
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @param string $email
     */
    public function setEmail(string $email): void
    {
        $this->email = $email;
    }

    /**
     * @return string
     */
    public function getPseudo(): string
    {
        return $this->pseudo;
    }

    /**
     * @param string $pseudo
     */
    public function setPseudo(string $pseudo): void
    {
        $this->pseudo = $pseudo;
    }

    /**
     * @return string
     */
    public function getMdp(): string
    {
        return $this->mdp;
    }

    /**
     * @param string $mdp
     */
    public function setMdp(string $mdp): void
    {
        $this->mdp = $mdp;
    }

    /**
     * @param bool $estAdmin
     */
    public function getEstAdmin(): bool
    {
        return $this->estAdmin;
    }
    /**
     * @param bool $estAdmin
     */
    public function setEstAdmin(bool $estAdmin): void
    {
        $this->estAdmin = $estAdmin;
    }
}