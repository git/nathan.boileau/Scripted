<?php

class Validation
{
    /**
     * If the variable is set, return true, otherwise return false.
     * 
     * @param var The variable you want to check if it's set.
     */
    public function ValidateSet($var) :bool{
        if (isset($var)) {
            return true;
        }
        return false;
    }

    /**
     * If the variable is empty, return false. Otherwise, return true.
     * 
     * @param var The variable to be validated.
     */
    public function ValidateNotEmpty($var) :bool{
        if (empty($var)) {
            return false;
        }
        return true;
    }

    /**
     * If the URL is not a valid URL, return false. Otherwise, return true.
     * 
     * @param string url The URL to be validated.
     * 
     * @return bool A boolean value.
     */
    public function ValidateURL(string $url) :bool{
        if (!preg_match("/\b(?:(?:https?|ftp):\/\/|www\.)[-a-z0-9+&@#\/%?=~_|!:,.;]*[-a-z0-9+&@#\/%=~_|]/i",$url)) {
            return false;
        }
        return true;
    }

    /**
     * It checks if the email is valid.
     * 
     * @param string email The email address to validate.
     * 
     * @return bool A boolean value.
     */
    public function ValidateEmail(string $email) :bool{
        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            return false;
        }
        return true;
    }

    /**
     * It returns true if the username is valid, and false if it isn't.
     * 
     * @param string username The username to validate.
     * 
     * @return bool A boolean value.
     */
    public function ValidateUsername(string $username) : bool{
      
        // if(!filter_var($username,FILTER_VALIDATE_REGEXP,array("options" => array( "regexp" => "^[^&=_'\-+;<>.]{1,18}$" ))))
        // {
        //     return false;
        // }
        return true;
    }

    /**
     * If the password is longer than 100 characters, return false. Otherwise, return true.
     * 
     * @param string password The password to validate.
     * 
     * @return bool A boolean value.
     */
    public function ValidatePassword(string $password) : bool{
        if(strlen($password)>100)
        {
            return false;
        }
        return true;
    }

    /**
     * It checks if the length of the strings are less than 250 characters and if the resolution time
     * and points are positive
     * 
     * @param string name The name of the enigma
     * @param string statement The statement of the enigma
     * @param string help Help text for the enigma
     * @param string reminder a reminder of the enigma
     * @param string example 
     * @param string solution The solution to the enigma
     * @param string test the test to be run on the solution
     * @param string resolutionTime the time it takes to solve the enigma
     * @param int points
     * 
     * @return bool a boolean value.
     */
    public function ValidateEnigme(string $name,string $statement,string $help,string $reminder,string $example,string $solution,string $test, string $resolutionTime, int $points) : bool{
        if(strlen($name)>50 || strlen($statement) > 250 || strlen($help) > 250 || strlen($reminder) > 250 || strlen($example) > 250 || strlen($solution) > 250 || strlen($test) > 250 || $resolutionTime < 0 || $points < 0)
            return false;
        return true;
    }
}
