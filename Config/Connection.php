<?php
class Connection extends SQLite3
{
    private $stmt;
    private $result;

    /**
     * The function __construct() is a constructor function that takes a parameter  and calls the
     * open() function with the parameter 
     * 
     * @param dsn The Data Source Name, or DSN, contains the information required to connect to the
     * database.
     */
    function __construct($dsn)
    {
        $this->open($dsn);
        $this->enableExceptions(true);
    }

    /**
     * It takes a query and an array of parameters, binds the parameters to the query, and executes the
     * query
     * 
     * @param string query The query to execute.
     * @param array parameters
     * 
     * @return bool The result of the query.
     */
    public function executeQuery(string $query, array $parameters = []): bool
    {
        $this->stmt = parent::prepare($query);
        foreach ($parameters as $name => $value) {
            $this->stmt->bindValue($name, $value[0], $value[1]);
        }
        $this->result = $this->stmt->execute();
        if ($this->result == false) {
            $this->result->finalize();
            return false;
        } else {
            $this->result->finalize();
            return true;
        }
    }


    /**
     * It takes the result of a query and returns an array of arrays
     * 
     * @return array An array of arrays.
     */
    public function getResults(): array
    {
        $resultArray = $this->result->fetchArray(SQLITE3_ASSOC);
        $multiArray = array();
        while($resultArray != false){ //read next row
            $multiArray[]=$resultArray;
            $resultArray = $this->result->fetchArray(SQLITE3_ASSOC); //insert all rows to $multiArray
        }
        if ($multiArray == NULL)
            return array();
        else
            return $multiArray;
    }
}