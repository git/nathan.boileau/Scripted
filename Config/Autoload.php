<?php

class Autoload
{
        private static $_instance = null;

        /**
         * If the class is not already loaded, then load it.
         */
        public static function charger()
    {
        if(null !== self::$_instance) {
            throw new RuntimeException(sprintf('%s is already started', __CLASS__));
        }

        self::$_instance = new self();


        if(!spl_autoload_register(array(self::$_instance, '_autoload'), false)) {
            throw new RuntimeException(sprintf('%s : Could not start the autoload', __CLASS__));
        }
    }

        /**
         * If the instance is not null, unregister the autoload function and set the instance to null.
         */
        public static function shutDown()
    {
        if(null !== self::$_instance) {

            if(!spl_autoload_unregister(array(self::$_instance, '_autoload'))) {
                throw new RuntimeException('Could not stop the autoload');
            }

            self::$_instance = null;
        }
    }

        /**
         * If the file exists, include it
         * 
         * @param class The name of the class to load.
         */
        private static function _autoload($class)
    {
        global $rep;
        $filename = $class.'.php';
        $dir =array('Model/','Handler/','./','Config/','Client/','Controller/','Metier/', 'Factory/');
        foreach ($dir as $d){
        $file=$rep.$d.$filename;
        if (file_exists($file))
        {
            include $file;
        }
        }
    
    }
}


?>