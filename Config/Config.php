<?php
//préfixe
$rep = '';

// BD
$dsn = './scripted.db';
// $dsn = 'C:\\wamp64\\www\\scripted.db';

//Sel de hashage
$sel = "JeSuisUnSeldeHashageEtJeSuisUniqueEtTresSecuriseEtJeSuisTresLong";

//View
//Page
$vues['main'] = 'View/src/pages/Main.php';
$vues['presentation'] = 'View/src/pages/Presentation.html';

//LogSign
$vues['login'] = 'View/src/pages/LogSign/Login.php';
$vues['signUp'] = 'View/src/pages/LogSign/SignUp.php';
$vues['mail'] = 'View/src/pages/LogSign/Mail.php';
$vues['confirm'] = 'View/src/pages/LogSign/Confirm.php';

//Test
$vues['test'] = 'View/src/pages/FirstTests/FirstTest1.php';
$vues['next'] = 'View/src/pages/FirstTests/FirstTest';

//Admin
$vues['admin'] = 'View/src/pages/Admin/Admin.php';
$vues['addEnigmeSolo'] = 'View/src/pages/Admin/AddEnigmeSolo.php';
$vues['addEnigmeMulti'] = 'View/src/pages/Admin/AddEnigmeMulti.php';
$vues['detailEnigme'] = 'View/src/pages/Admin/DetailEnigme.php';
$vues['modifEnigmeSolo'] = 'View/src/pages/Admin/ModifEnigmeSolo.php';
$vues['modifEnigmeMulti'] = 'View/src/pages/Admin/ModifEnigmeMulti.php';
$vues['adminSolo'] = 'View/src/pages/Admin/AdminExtendSolo.php';
$vues['adminMulti'] = 'View/src/pages/Admin/AdminExtendMulti.php';
$vues['seeOrdre'] = 'View/src/pages/Admin/SeeOrdre.php';
$vues['modifOrdre'] = 'View/src/pages/Admin/ModifOrdre.php';

//Mulijoueur
$vues['partie'] = 'View/src/pages/Multijoueur/Partie.php';
$vues['queue'] = 'View/src/pages/Multijoueur/FileAttente.php';
$vues['lobby'] = 'View/src/pages/Multijoueur/Lobby.php';
$vues['lobbyEnd'] = 'View/src/pages/Multijoueur/LobbyEnd.php';
$vues['gameEnd'] = 'View/src/pages/Multijoueur/GameEnd.php';
$vues['dashboard'] = 'View/src/pages/Multijoueur/Dashboard.html';

// Enigme
$vues['enigmePage'] = 'View/src/pages/Enigme/EnigmePage.php';

//Error
$vues['erreur'] = 'View/src/pages/Erreur.php';
$error = "";

// Server
$serverAdress = "82.165.180.114";
$playerNumberPerGame = 2;
$nbEnigmePerGame = 2;