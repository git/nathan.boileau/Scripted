<?php
Class Nettoyage{
    /**
     * It takes a string, trims it, strips it of HTML tags, and returns the string
     * 
     * @param string input The string to be cleaned.
     * 
     * @return string The output of the function.
     */
    public function clean($input)
    {
        $output = trim($input);
        $output = strip_tags($output);
        // $output = htmlspecialchars($output);

        return $output;
    }
    /**
     * It takes a string, cleans it, then sanitizes it as an email.
     * 
     * @param string input The input to be cleaned.
     * 
     * @return string The output is being returned.
     */
    public function cleanEmail($input){
        $output = $this->clean($input);
        $output = filter_var($output, FILTER_SANITIZE_EMAIL);
        return $output;
    }

    public function cleanInt($input){
        $output = $this->clean($input);
        $output = filter_var($output, FILTER_SANITIZE_NUMBER_INT);
        return $output;
    }
}