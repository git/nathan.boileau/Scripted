<?php
class VisitorModel
{

    private EnigmeGateway $enigme_gateway;
    private PartieGateway $partie_gateway;
    private UtilisateurGateway $utilisateur_gateway;
    private Nettoyage $nettoyage;
    private Validation $validation;

    function __construct()
    {
        try {
            global $error, $view, $rep;
            $this->enigme_gateway = new EnigmeGateway();
            $this->partie_gateway = new PartieGateway();
            $this->utilisateur_gateway = new UtilisateurGateway();
            $this->nettoyage = new Nettoyage();
            $this->validation = new Validation();
        } catch (Exception $e) {
            $error = $e->getMessage();
            require($rep . $view['erreur']);
        }
    }

    public function signUp()
    {
        global $sel, $error;
        $email = $this->nettoyage->cleanEmail($_REQUEST['email']);
        if (!$this->validation->ValidateEmail($email)) {
            $error = "Email invalides.";
            throw (new Exception("Email non valide"));
        }
        $username = $this->nettoyage->clean($_REQUEST['username']);
        if (!$this->validation->ValidateUsername($username)) {
            $error = "Nom d'utilisateur invalides. Il ne doit pas contenir de caractère spéciaux.";
            throw (new Exception("Pseudo non valide"));
        }
        $password = $this->nettoyage->clean($_REQUEST['password']);
        if (!$this->validation->ValidatePassword($password)) {
            $error = "Mots de passe invalides. Il ne doit pas dépasser 100 caractères.";
            throw (new Exception("Mot de passe non valide"));
        }
        $j = $this->utilisateur_gateway->getUtilisateurByEmail($email);
        if ($j->getEmail() != "null") {
            $error = "Email déjà utilisé.";
            throw (new Exception("Email déjà utilisé"));
        }
        $password = password_hash($password . $sel, PASSWORD_DEFAULT);
        $utilisateur = new Utilisateur($email, $username, $password, false);
        $this->utilisateur_gateway->insert($utilisateur);
        $_SESSION['role'] = 'user';
        $_SESSION['utilisateur'] = $utilisateur;
        $_SESSION['pseudo'] = $utilisateur->getPseudo();
    }
    public function login()
    {
        global $vue, $sel, $error;
        $email = $this->nettoyage->cleanEmail($_REQUEST['email']);
        if (!$this->validation->ValidateEmail($email)) {
            $error = "Email invalides.";
            throw (new Exception("Email non valide"));
        }
        $utilisateur = $this->utilisateur_gateway->getUtilisateurByEmail($email);
        if ($utilisateur->getEmail() == "null") {
            $error = "Utilisateur non trouvé.";
            throw new Exception("Utilisateur introuvable");
        }
        $mdp = $this->utilisateur_gateway->getMdpByEmail($email);
        if (! password_verify($_REQUEST['password'] . $sel, $mdp)) {
            $error = "Mot de passe incorrect.";
            throw new Exception("Mot de passe invalide");
        }
        $estAdmin = $this->utilisateur_gateway->getEstAdminByEmail($email);
        if ($estAdmin == true) {
            $_SESSION['role'] = "admin";
        } else {
            $_SESSION['role'] = "user";
        }
        $_SESSION['utilisateur'] = $utilisateur;
        $_SESSION['connected'] = 'true';
        $_SESSION['pseudo'] = $utilisateur->getPseudo();
    }
}