<?php
class EnigmeGateway
{
    private Connection $con;

    /**
     * @param Connection $con
     */

    public function __construct()
    {
        global $dsn, $rep, $vues, $error;
        try{
        $con = new Connection($dsn);
        $this->con = $con;
        } catch (Exception $e) {
            $error = $e->getMessage();
            require($rep . $vues['erreur']);
        }
    }

    /**
     * @param Connection $con
     */
    public function setCon(Connection $con): void
    {
        $this->con = $con;
    }

    /**
     * It inserts a new row in the Enigme table, with the values of the Enigme object passed as
     * parameter
     * 
     * @param Enigme enigme
     */
    public function insert(Enigme $enigme)
    {
        $query = "INSERT INTO Enigme VALUES (NULL,:nom,:enonce,:aide,:rappel,:exemple,:solution,:test, :ordre,:tempsDeResolution,:points, :prompt)";
        $this->con->executeQuery($query, array(
            ':nom' => array($enigme->getNom(), SQLITE3_TEXT),
            ':enonce' => array($enigme->getEnonce(), SQLITE3_TEXT),
            ':aide' => array($enigme->getAide(), SQLITE3_TEXT),
            ':rappel' => array($enigme->getRappel(), SQLITE3_TEXT),
            ':exemple' => array($enigme->getExemple(), SQLITE3_TEXT),
            ':solution' => array($enigme->getSolution(), SQLITE3_TEXT),
            ':test' => array($enigme->getTest(), SQLITE3_TEXT),
            ':ordre' => array($enigme->getOrdre(), SQLITE3_INTEGER),
            ':tempsDeResolution' => array($enigme->getTempsDeResolution(), SQLITE3_INTEGER),
            ':points' => array($enigme->getPoints(), SQLITE3_INTEGER),
            ':prompt' => array($enigme->getPrompt(), SQLITE3_TEXT)
        ));
    }

    // function that upadate enigme in database
    public function update(Enigme $enigme)
    {
        $query = "UPDATE Enigme SET nom=:nom, enonce=:enonce, aide=:aide, rappel=:rappel, exemple=:exemple, solution=:solution, test=:test, ordre=:ordre, tempsDeResolution=:tempsDeResolution, points=:points, prompt=:prompt WHERE id=:id";
        $this->con->executeQuery($query, array(
            ':id' => array($enigme->getIdEnigme(), SQLITE3_INTEGER),
            ':nom' => array($enigme->getNom(), SQLITE3_TEXT),
            ':enonce' => array($enigme->getEnonce(), SQLITE3_TEXT),
            ':aide' => array($enigme->getAide(), SQLITE3_TEXT),
            ':rappel' => array($enigme->getRappel(), SQLITE3_TEXT),
            ':exemple' => array($enigme->getExemple(), SQLITE3_TEXT),
            ':solution' => array($enigme->getSolution(), SQLITE3_TEXT),
            ':test' => array($enigme->getTest(), SQLITE3_TEXT),
            ':ordre' => array($enigme->getOrdre(), SQLITE3_INTEGER),
            ':tempsDeResolution' => array($enigme->getTempsDeResolution(), SQLITE3_INTEGER),
            ':points' => array($enigme->getPoints(), SQLITE3_INTEGER),
            ':prompt' => array($enigme->getPrompt(), SQLITE3_TEXT)
        ));
    }

    /**
     * It deletes a row from the table Enigme where the idEnigme is equal to the idEnigme passed as a
     * parameter
     * 
     * @param string idEnigme the id of the enigma
     */
    public function delete(string $idEnigme)
    {
        $query= "DELETE FROM Enigme WHERE id=:idEnigme";
        $this->con->executequery($query, array(
            ':idEnigme' => array($idEnigme,SQLITE3_INTEGER)
        ));
    }

    /**
     * It returns an array of Enigme objects
     * For multiplayer Enigma
     * 
     * @return array An array of Enigme objects.
     */
    public function findMultiEnigma() : array
    {
        $query = "SELECT * FROM Enigme
        WHERE points != 0
        AND tempsDeResolution IS NOT NULL OR tempsDeResolution != 0";
        $this->con->executeQuery($query);
        $tabEnigme=EnigmeFactory::create($this->con->getResults());
        return $tabEnigme;
    }

    /**
     * It returns an array of Enigma objects
     * For Solo enigma
     * 
     * @return array of objects.
     */
    public function findSoloEnigma() : array
    {
        $query = "SELECT * FROM Enigme
        WHERE points IS NULL OR points = 0";
        $this->con->executeQuery($query);
        $tabEnigme=EnigmeFactory::create($this->con->getResults());
        return $tabEnigme;
    }

    public function findEnigmaFromPartie(string $idPartie) : array{
        $query = "SELECT * FROM Enigme e, Contenir c
        WHERE c.partie=:idPartie";
        $this->con->executeQuery($query, array(
            'idPartie' => array($idPartie, SQLITE3_TEXT)
        ));
        $tabEnigme=EnigmeFactory::create($this->con->getResults());
        return $tabEnigme;
    }

    public function findById(int $idEnigme) : array
    {
        $query="SELECT * FROM Enigme WHERE id =:id";
        $this->con->executequery($query,array(
                ':id' => array($idEnigme,SQLITE3_INTEGER)
        ));
        $results=$this->con->getResults();
        $enigme=EnigmeFactory::create($results);
        return $enigme;
    }

    public function findByTempsDeResolution() : array
    {
        $query = "SELECT * FROM Enigme ORDER BY tempsDeResolution";
        $this->con->executequery($query);
        $results = $this->con->getResults();
        $tabEnigme=EnigmeFactory::create($results);
        return $tabEnigme;
    }
    public function findLastEnigma() : array
    {
        $query = "SELECT * FROM Enigme ORDER BY id DESC LIMIT 1";
        $this->con->executequery($query);
        $results = $this->con->getResults();
        $tabEnigme=EnigmeFactory::create($results);
        return $tabEnigme;
    }
    public function findLastEnigmaByOrdre() : array
    {
        $query = "SELECT * FROM Enigme ORDER BY ordre DESC LIMIT 1";
        $this->con->executequery($query);
        $results = $this->con->getResults();
        $tabEnigme=EnigmeFactory::create($results);
        return $tabEnigme;
    }

    public function getLastOrdre() : int
    {
        $query = "SELECT ordre FROM Enigme ORDER BY ordre DESC LIMIT 1";
        $this->con->executequery($query);
        $results = $this->con->getResults();
        return $results[0]['ordre'];
    }
    public function findByOrdre(int $ordre) : array
    {
        $query = "SELECT * FROM Enigme WHERE ordre = :ordre";
        $this->con->executequery($query,array(
            ':ordre' => array($ordre,SQLITE3_INTEGER)
        ));
        $results = $this->con->getResults();
        $tabEnigme=EnigmeFactory::create($results);
        return $tabEnigme;
    }

    public function getRandomEnigme() : Enigme
    {
        $query = "SELECT * FROM Enigme 
        WHERE points != 0
        AND tempsDeResolution != 0
        AND ordre = 0
        ORDER BY RANDOM() LIMIT 1";
        $this->con->executequery($query);
        $results = $this->con->getResults();
        if (empty($results)){
            throw new Exception("Aucune énigme mulitijoeur n'est disponible"); 
        }
        $tabEnigme=EnigmeFactory::create($results);
        return $tabEnigme[0];
    }
    public function showAll(): void
    {
        $query = "SELECT * FROM Enigme";
        $this->con->executeQuery($query);
        $results = $this->con->getResults();
        foreach ($results as $row) {
            echo $row['idEnigme'] . '</br>';
            echo $row['nom'] . '</br>';
            echo $row['enonce'] . '</br>';
            echo $row['aide'] . '</br>';
            echo $row['rappel'] . '</br>';
            echo $row['solution'] . '</br>';
            echo $row['test'] . '</br>';
            echo $row['points'] . '</br>';
        }
    }
}
?>