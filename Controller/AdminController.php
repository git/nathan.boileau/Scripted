<?php

class AdminController extends UserController
{
    function __construct()
    {
        try {
            global $dsn, $rep, $vues, $error;
            $action = $_REQUEST['action'];
            switch ($action) {
                case NULL:
                    $this->goToHome();
                    break;
                case "goToHome":
                    $this->goToHome();
                    break;
                case "goToAdmin":
                    $this->goToAdmin();
                    break;
                case "goToPresentation":
                    $this->goToPresentation();
                    break;
                case "goToSolo":
                    $this->goToSolo();
                    break;
                case "goToEnigme":
                    $this->goToEnigme();
                    break;
                case "goToTest":
                    $this->goToTest();
                    break;
                case "goToNext":
                    $this->goToNext();
                    break;
                case "goToAddEnigmeSolo":
                    $this->goToAddEnigmeSolo();
                    break;
                case "goToAddEnigmeMulti":
                    $this->goToAddEnigmeMulti();
                    break;
                case "logout":
                    $this->logout();
                    break;
                case "addNewEnigmeSolo":
                    $this->addNewEnigmeSolo();
                    break;
                case "addNewEnigmeMulti":
                    $this->addNewEnigmeMulti();
                    break;
                case "goToDetailEnigme":
                    $this->goToDetailEnigme();
                    break;
                case "goToModifEnigmeSolo":
                    $this->goToModifEnigmeSolo();
                    break;
                case "goToModifEnigmeMulti":
                    $this->goToModifEnigmeMulti();
                    break;
                case "goToAdminSolo":
                    $this->goToAdminSolo();
                    break;
                case "goToAdminMulti":
                    $this->goToAdminMulti();
                    break;
                case "goToSeeOrdre":
                    $this->goToSeeOrdre();
                    break;
                case "goToModifOrdre":
                    $this->goToModifOrdre();
                    break;
                case "enigmeEnded":
                    $this->enigmeEnded();
                    break;
                case "editEnigme":
                    $this->editEnigme();
                    break;
                case "deleteEnigme":
                    $this->deleteEnigme();
                    break;
                case "modifOrdre":
                    $this->modifOrdre();
                    break;
                case "goToGame":
                    $this->goToGame();
                    break;
                case "enigmeMultiEnded":
                    $this->enigmeMultiEnded();
                    break;
                case "goToLobby":
                    $this->goToLobby();
                    break;
                case "waiting":
                    $this->waiting();
                    break;
                case "addToQueue":
                    $this->addToQueue();
                    break;
                case "saveCode":
                    $this->saveCode();
                    break;
                case "saveCodeMulti":
                    $this->saveCodeMulti();
                    break;
                case "saveCodeInCookie":
                    $this->saveCodeInCookie();
                    break;
                case "getGameEtat":
                    $this->getGameEtat();
                    break;
                case "endGame":
                    $this->endGame();
                    break;
                case "quitQueue":
                    $this->quitQueue();
                    break;
                case "quitGame":
                    $this->quitGame();
                    break;
                case "skipEnigme":
                    $this->skipEnigme();
                    break;
                case "goToDashboard":
                    $this->goToDashboard();
                    break;
                case "getPlayersPseudo":
                    $this->getPlayersPseudo();
                    break;
                case "getDashboardInfo":
                    $this->getDashboardInfo();
                    break;
                case "report":
                    $this->report();
                    break;
                default:
                    $error = "Action non valide";
                    require($rep . $vues['erreur']);
                    break;
            }
        } catch (PDOException $e) {
            $error = $e->getMessage();
            require($rep . $vues['erreur']);
        } catch (Exception $e2) {
            $error = $e2->getMessage();
            require($rep . $vues['erreur']);
        }
    }

    /**
     * It gets the enigme from the database and displays it
     * in the enigme page.
     */
    public function goToEnigme()
    {
        try {
            global $rep, $vues;
            $model = new UserModel();
            $ordre = $_REQUEST['ordre'];
            $enigme = $model->getEnigmeByOrdre($ordre);
            if ($enigme->getNom() == ""){
                $enigme = $model->getEnigmeByOrdre($ordre-1);
            }
            require($rep . $vues['enigmePage']);
        } catch (Exception $e) {
            $error = $e->getMessage();
            require($rep . $vues['erreur']);
        }
    }

    /**
     * It gets the enigmas from the database and sorts them by their order
     * and displays them in the admin page.
     * 
     */
    public function goToAdmin()
    {
        try {
            global $rep, $vues;
            $model = new AdminModel();
            $lesEnigmesSolo = $model->getEnigmesSolo();
            usort($lesEnigmesSolo, function($a, $b) {
                return $a->getOrdre() - $b->getOrdre();
            });
            $lesEnigmesMulti = $model->getEnigmesMulti();
            require($rep . $vues['admin']);
        } catch (Exception $e) {
            $error = $e->getMessage();
            require($rep . $vues['erreur']);
        }
    }

    public function goToAddEnigmeSolo()
    {
        try {
            global $rep, $vues;
            require($rep . $vues['addEnigmeSolo']);
        } catch (Exception $e) {
            $error = $e->getMessage();
            require($rep . $vues['erreur']);
        }
    }
    
    public function goToAddEnigmeMulti(){
        try {
            global $rep, $vues;
            require($rep . $vues['addEnigmeMulti']);
        } catch (Exception $e) {
            $error = $e->getMessage();
            require($rep . $vues['erreur']);
        }
    }
    public function goToDetailEnigme()
    {
        try {
            global $rep, $vues;
            $model = new AdminModel();
            $enigme = $model->getEnigmeById($_GET['id']);
            require($rep . $vues['detailEnigme']);
        } catch (Exception $e) {
            $error = $e->getMessage();
            require($rep . $vues['erreur']);
        }
    }
    public function goToModifEnigmeSolo()
    {
        try {
            global $rep, $vues;
            $model = new AdminModel();
            $enigme = $model->getEnigmeById($_GET['id']);
            require($rep . $vues['modifEnigmeSolo']);
        } catch (Exception $e) {
            $error = $e->getMessage();
            require($rep . $vues['erreur']);
        }
    }
    public function goToModifEnigmeMulti()
    {
        try {
            global $rep, $vues;
            $model = new AdminModel();
            $enigme = $model->getEnigmeById($_GET['id']);
            require($rep . $vues['modifEnigmeMulti']);
        } catch (Exception $e) {
            $error = $e->getMessage();
            require($rep . $vues['erreur']);
        }
    }
    public function goToAdminSolo()
    {
        try {
            global $rep, $vues;
            $model = new AdminModel();
            $lesEnigmesSolo = $model->getEnigmesSolo();
            usort($lesEnigmesSolo, function($a, $b) {
                return $a->getOrdre() - $b->getOrdre();
            });
            require($rep . $vues['adminSolo']);
        } catch (Exception $e) {
            $error = $e->getMessage();
            require($rep . $vues['erreur']);
        }
    }
    public function goToAdminMulti()
    {
        try {
            global $rep, $vues;
            $model = new AdminModel();
            $lesEnigmesMulti = $model->getEnigmesMulti();
            require($rep . $vues['adminMulti']);
        } catch (Exception $e) {
            $error = $e->getMessage();
            require($rep . $vues['erreur']);
        }
    }
    public function goToSeeOrdre()
    {
        try {
            global $rep, $vues;
            $model = new AdminModel();
            $lesEnigmes = $model->getEnigmesSolo();
            usort($lesEnigmes, function($a, $b) {
                return $a->getOrdre() - $b->getOrdre();
            });

            require($rep . $vues['seeOrdre']);
        } catch (Exception $e) {
            $error = $e->getMessage();
            require($rep . $vues['erreur']);
        }
    }
    public function goToModifOrdre()
    {
        try {
            global $rep, $vues;
            $model = new AdminModel();
            $lesEnigmes = $model->getEnigmesSolo();
            usort($lesEnigmes, function($a, $b) {
                return $a->getOrdre() - $b->getOrdre();
            });
            require($rep . $vues['modifOrdre']);
        } catch (Exception $e) {
            $error = $e->getMessage();
            require($rep . $vues['erreur']);
        }
    }
    /**
     * When the user clicks on the button, the function enigmeEnded() is called, which will display the
     * next enigma.
     */
    public function enigmeEnded(){
        try{
            global $rep, $vues;
            $model = new UserModel();
            $ordre = $_REQUEST['ordre']+1;
            $enigme = $model->getEnigmeByOrdre($ordre);
            require($rep . $vues['enigmePage']);
        } catch (Exception $e) {
            $error = $e->getMessage();
            require($rep . $vues['erreur']);
        }
    }

    public function editEnigme()
    {
        try{
            global $rep, $vues;
            $model = new AdminModel();
            $id = $_GET['id'];
            $nom = $_POST['nom'];
            $enonce = $_POST['enigme'];
            $aide = $_POST['aide'];
            $rappel = $_POST['rappel'];
            $exemple = $_POST['exemple'];
            $test = $_POST['test'];
            $solution = $_POST['solution'];
            $prompt = $_POST['prompt'];
            if (isset($_POST['points'])) {
                $points = $_POST['points'];
            } else {
                $points = 0;
            }
            if (isset($_POST['tempsDeResolution'])){
                $tempsDeResolution = $_POST['tempsDeResolution'];
            } else {
                $tempsDeResolution = 0;
            }
            $enigme = $model->editEnigme($id, $nom, $enonce, $aide, $rappel, $exemple, $test, $solution, $prompt, $points, $tempsDeResolution);
            echo '<script>alert("L\'énigme a bien été modifier.");</script>';
            require($rep . $vues['detailEnigme']);
        }
        catch (Exception $e) {
            $error = $e->getMessage();
            require($rep . $vues['erreur']);
        }
    }

    public function deleteEnigme(){
        try{
            global $rep, $vues;
            $model = new AdminModel();
            $id = $_GET['id'];
            $model->deleteEnigme($id);
            echo '<script>alert("L\'énigme a bien été supprimé.");</script>';
            $this->goToAdmin();
        }
        catch (Exception $e) {
            $error = $e->getMessage();
            require($rep . $vues['erreur']);
        }
    }
    public function modifOrdre(){
        try{
            global $rep, $vues;
            $model = new AdminModel();
            $enigme = $_GET['id'];
            $dir = $_GET['dir'];
            if ($dir == 'up'){
                $model->modifOrdreUp($enigme);
            } else {
                $model->modifOrdreDown($enigme);
            }
            $this->goToSeeOrdre();
        }
        catch (Exception $e) {
            $error = $e->getMessage();
            require($rep . $vues['erreur']);
        }
    }

    public function addNewEnigmeSolo()
    {
        try {
            global $rep, $vues;
            $model = new AdminModel();
            $nom = $_POST['nom'];
            $enonce = $_POST['enigme'];
            $aide = $_POST['aide'];
            $rappel = $_POST['rappel'];
            $exemple = $_POST['exemple'];
            $test = $_POST['test'];
            $solution = $_POST['solution'];
            $prompt = $_POST['prompt'];
            if (empty($nom) || empty($enonce) || empty($test) || empty($solution)) {
                throw new Exception("Les champs nom, enigme, test et solution doivent être remplis");
            }
            if (empty($aide)) {
                $aide = "Il n'y a pas d'aide pour cette énigme";
            }
            if (empty($rappel)) {
                $rappel = "Il n'y a pas de rappel pour cette énigme";
            }
            if (empty($exemple)) {
                $exemple = "Il n'y a pas d'exemple pour cette énigme";
            }
            $enigme = $model->addNewEnigmeSolo($nom, $enonce, $aide, $rappel, $exemple, $test, $solution, $prompt);
            echo '<script>alert("L\'énigme a bien été ajouté.");</script>';
            $this->goToAdmin();
        } catch (Exception $e) {
            $error = $e->getMessage();
            require($rep . $vues['erreur']);
        }
    }
    public function addNewEnigmeMulti()
    {
        try {
            global $rep, $vues;
            $model = new AdminModel();
            $nom = $_POST['nom'];
            $enonce = $_POST['enigme'];
            $aide = $_POST['aide'];
            $rappel = $_POST['rappel'];
            $exemple = $_POST['exemple'];
            $test = $_POST['test'];
            $solution = $_POST['solution'];
            $prompt = $_POST['prompt'];
            $points = $_POST['points'];
            $tempsDeResolution = $_POST['tempsDeResolution'];
            if (empty($nom) || empty($enonce) || empty($test) || empty($solution) || empty($points) || empty($tempsDeResolution)) {
                throw new Exception("Les champs nom, enigme, test, solution, points et temps de résolution doivent être remplis");
            }
            if (empty($aide)) {
                $aide = "Il n'y a pas d'aide pour cette énigme";
            }
            if (empty($rappel)) {
                $rappel = "Il n'y a pas de rappel pour cette énigme";
            }
            if (empty($exemple)) {
                $exemple = "Il n'y a pas d'exemple pour cette énigme";
            }
            $enigme = $model->addNewEnigmeMulti($nom, $enonce, $aide, $rappel, $exemple, $test, $solution, $prompt, $points, $tempsDeResolution);
            echo '<script>alert("L\'énigme a bien été ajouté.");</script>';
            $this->goToAdmin();
        } catch (Exception $e) {
            $error = $e->getMessage();
            require($rep . $vues['erreur']);
        }
    }
    
    public function report(){
        try{
            global $rep, $vues;
            echo "<script>alert('Cette fonctionalité est en cours de développement.')</script>";
            $this->goToAdmin();
        }
        catch (Exception $e) {
            $error = $e->getMessage();
            require($rep . $vues['erreur']);
        }
    }
}