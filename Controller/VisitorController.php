<?php

class VisitorController
{
    private VisitorModel $model;
    function __construct()
    {
        try {
            global $dsn, $rep, $vues, $error;
            $this->model = new VisitorModel();
            $action = $_REQUEST['action'];
            switch ($action) {
                case NULL:
                    $this->goToHome();
                    break;
                case "signUp":
                    $this->signUp();
                    break;
                case "login":
                    $this->login();
                    break;
                case "goToHome":
                    $this->goToHome();
                    break;
                case "goToLogin":
                    $this->goToLogin();
                    break;
                case "goToSignUp":
                    $this->goToSignUp();
                    break;
                case "goToMail":
                    $this->goToMail();
                    break;
                case "goToQueue":
                    $this->goToLogin();
                    break;
                case "sendMail":
                    $this->sendMail();
                    break;
                case "reset":
                    $this->reset();
                    break;
                default:
                    $error = "Action non valide. Pour accéder à cette page, vous devez peut être vous connecter";
                    require($rep . $vues['erreur']);
                    break;
            }
        } catch (PDOException $e) {
            $error = $e->getMessage();
            require($rep . $vues['erreur']);
        } catch (Exception $e2) {
            $error = $e2->getMessage();
            require($rep . $vues['erreur']);
        }
    }
    public function goToHome()
    {
        try {
            global $rep, $vues;
            require($rep . $vues['main']);
        } catch (Exception $e) {
            $error = $e->getMessage();
            require($rep . $vues['erreur']);
        }
    }
    public function goToLogin()
    {
        try {
            global $rep, $vues;
            require($rep . $vues['login']);
        } catch (Exception $e) {
            $error = $e->getMessage();
            require($rep . $vues['erreur']);
        }
    }
    public function goToSignUp()
    {
        try {
            global $rep, $vues;
            require($rep . $vues['signUp']);
        } catch (Exception $e) {
            $error = $e->getMessage();
            require($rep . $vues['erreur']);
        }
    }
    public function goToMail()
    {
        try {
            global $rep, $vues;
            require($rep . $vues['mail']);
        } catch (Exception $e) {
            $error = $e->getMessage();
            require($rep . $vues['erreur']);
        }
    }
    public function signUp()
    {
        try {
            global $rep, $vues, $error;
            $this->model->signUp();
            $this->goToHome();
        } catch (PDOException $e) {
            $error = "Erreur de connexion à la base de données.";
            require($rep . $vues['erreur']);
        } catch (Exception $e) {
            $error = $e->getMessage();
            require($rep . $vues['erreur']);
        }
    }
    public function login()
    {
        try {
            global $rep, $vues, $error;
            $this->model->login();
            $this->goToHome();
        } catch (PDOException $e) {
            $error = "Erreur de connexion à la base de données.";
            require($rep . $vues['erreur']);
        } catch (Exception $e) {
            $error = $e->getMessage();
            require($rep . $vues['erreur']);
        }
    }

    public function sendMail()
    {
        $nettoyage = new Nettoyage();
        $code = 10;
        $to = $nettoyage->cleanEmail($_POST['email']);
        $subject = 'Reset your password';
        $message = 'Hello, you can reset your password here : http://82.165.180.114/index.php?action=goToConfirm&code='.$code;
        $headers = 'From: scripted@gmail.com' . "\r\n" .
            // 'Reply-To: sender@example.com' . "\r\n" .
            'X-Mailer: PHP/' . phpversion();

        mail($to, $subject, $message, $headers);
        $this->goToMail();
    }

    public function goToConfirm()
    {
        try {
            global $rep, $vues;
            require($rep . $vues['confirm']);
        } catch (Exception $e) {
            $error = $e->getMessage();
            require($rep . $vues['erreur']);
        }
    }

    public function reset()
    {
        try {
            global $rep, $vues, $error;
            echo "<script>alert('Cette fonctionalité est en cours de développement.')</script>";
            require($rep . $vues['login']);
        } catch (Exception $e) {
            $error = $e->getMessage();
            require($rep . $vues['erreur']);
        }
    }
}