<?php
class UserController
{
    function __construct()
    {
        try {
            global $dsn, $rep, $vues, $error;
            $action = $_REQUEST['action'];
            switch ($action) {
                case NULL:
                    $this->goToHome();
                    break;
                case "goToHome":
                    $this->goToHome();
                    break;
                case "goToPresentation":
                    $this->goToPresentation();
                    break;
                case "goToSolo":
                    $this->goToSolo();
                    break;
                case "goToEnigme":
                    $this->goToEnigme();
                    break;
                case "goToTest":
                    $this->goToTest();
                    break;
                case "goToNext":
                    $this->goToNext();
                    break;
                case "goToGame":
                    $this->goToGame();
                    break;
                case "enigmeEnded":
                    $this->enigmeEnded();
                    break;
                case "enigmeMultiEnded":
                    $this->enigmeMultiEnded();
                    break;
                case "goToLobby":
                    $this->goToLobby();
                    break;
                case "waiting":
                    $this->waiting();
                    break;
                case "addToQueue":
                    $this->addToQueue();
                    break;
                case "logout":
                    $this->logout();
                    break;
                case "saveCode":
                    $this->saveCode();
                    break;
                case "saveCodeMulti":
                    $this->saveCodeMulti();
                    break;
                case "saveCodeInCookie":
                    $this->saveCodeInCookie();
                    break;
                case "getGameEtat":
                    $this->getGameEtat();
                    break;
                case "endGame":
                    $this->endGame();
                    break;
                case "quitQueue":
                    $this->quitQueue();
                    break;
                case "quitGame":
                    $this->quitGame();
                    break;
                case "skipEnigme":
                    $this->skipEnigme();
                    break;
                case "goToDashboard":
                    $this->goToDashboard();
                    break;
                case "getPlayersPseudo":
                    $this->getPlayersPseudo();
                    break;
                case "getDashboardInfo":
                    $this->getDashboardInfo();
                    break;
                default:
                    $error = "Action non valide";
                    require($rep . $vues['erreur']);
                    break;
            }
        } catch (PDOException $e) {
            $error = $e->getMessage();
            require($rep . $vues['erreur']);
        } catch (Exception $e2) {
            $error = $e2->getMessage();
            require($rep . $vues['erreur']);
        }
    }
    public function goToHome()
    {
        try {
            global $rep, $vues;
            require($rep . $vues['main']);
        } catch (Exception $e) {
            $error = $e->getMessage();
            require($rep . $vues['erreur']);
        }
    }
    public function goToPresentation()
    {
        try {
            global $rep, $vues;
            require($rep . $vues['presentation']);
        } catch (Exception $e) {
            $error = $e->getMessage();
            require($rep . $vues['erreur']);
        }
    }
    public function goToDashboard()
    {
        try {
            global $rep, $vues;
            require($rep . $vues['dashboard']);
        } catch (Exception $e) {
            $error = $e->getMessage();
            require($rep . $vues['erreur']);
        }
    }
    public function goToSolo()
    {
        try {
            global $rep, $vues;
            $model = new UserModel();
            $utilisateur = $_SESSION['utilisateur'];
            $enigme = $model->getLastEnigmeEnded($utilisateur->getEmail());
            header("Location: index.php?action=goToEnigme&ordre=" . ($enigme->getOrdre()+1));
        } catch (Exception $e) {
            $error = $e->getMessage();
            require($rep . $vues['erreur']);
        }
    }
    public function goToTest()
    {
        try {
            global $rep, $vues;
            require($rep . $vues['test']);
        } catch (Exception $e) {
            $error = $e->getMessage();
            require($rep . $vues['erreur']);
        }
    }
    public function goToEnigme()
    {
        try {
            global $rep, $vues;
            $model = new UserModel();
            $ordre = $_REQUEST['ordre'];
            $enigme = $model->getEnigmeByOrdre($ordre);
            if ($enigme->getNom() == "") {
                header("Location: index.php?action=goToHome");
            }
            $utilisateur=$_SESSION['utilisateur'];
            if ($ordre != 1){
                $lastEnigme = $model->getEnigmeByOrdre($ordre-1);
                if(! $model->checkEnigmeIsEnded($utilisateur->getEmail(), $lastEnigme->getIdEnigme())){
                    header("Location: index.php?action=goToEnigme&ordre=".($ordre-1));
                }
                else{
                    $model->resoudreEnigmeSolo($utilisateur, $enigme->getIdEnigme());
                    $code = $model->getCode($utilisateur->getEmail(), $enigme->getIdEnigme());
                    require($rep . $vues['enigmePage']);
                }
            }
            elseif($model->checkEnigmeIsEnded($utilisateur->getEmail(), $enigme->getIdEnigme())){
                $code = $model->getCode($utilisateur->getEmail(), $enigme->getIdEnigme());
                require($rep . $vues['enigmePage']);
            }
            else {
                $model->resoudreEnigmeSolo($utilisateur, $enigme->getIdEnigme());
                $code = $model->getCode($utilisateur->getEmail(), $enigme->getIdEnigme());
                require($rep . $vues['enigmePage']);
            }
        } catch (Exception $e) {
            $error = $e->getMessage();
            require($rep . $vues['erreur']);
        }
    }
    public function goToNext()
    {
        try {
            global $rep, $vues, $error;
            $nettoyage = new Nettoyage();
            $num = $nettoyage->cleanInt($_REQUEST['num']);
            require($rep . $vues['next'] . $num . ".php");
        } catch (Exception $e) {
            $error = "Erreur Inconnue";
            require($rep . $vues['erreur']);
        }
    }
    public function enigmeEnded(){
        try {
            global $rep, $vues;
            $model = new UserModel();
            $ordre = $_REQUEST['ordre'];
            $enigme = $model->getEnigmeByOrdre($ordre);
            $utilisateur=$_SESSION['utilisateur'];
            $model->enigmeEnded($utilisateur->getEmail(),$enigme->getIdEnigme());
            $_REQUEST['ordre'] = $ordre + 1;
            $lastOrdre = $model->getLastOrdre();
            if ($_REQUEST['ordre'] == $lastOrdre) {
                $this->goToHome();
            }
            else {
                header("Location: index.php?action=goToEnigme&ordre=" . $_REQUEST['ordre']);
            }
        } catch (Exception $e) {
            $error = "Erreur Inconnue";
            require($rep . $vues['erreur']);
        }
    }
    public function enigmeMultiEnded(){
        try {
            global $rep, $vues;
            $model = new UserModel();
            $index = $_REQUEST['index'];
            $enigme = $model->getEnigmebyPartieIdAndIndex($_SESSION['idPartie'],$index);
            $utilisateur=$_SESSION['utilisateur'];
            $model->enigmeMultiEnded($utilisateur->getEmail(),$enigme->getIdEnigme());
            $index = $index + 1;
            header("Location: index.php?action=goToGame&idPartie=" . $_SESSION['idPartie'] . "&index=". $index);
        } catch (Exception $e) {
            $error = "Erreur Inconnue";
            require($rep . $vues['erreur']);
        }
    }
    public function goToLobby()
    {
        try {
            global $rep, $vues, $error;
            require($rep . $vues['lobby']);
        } catch (Exception $e) {
            $error = $e->getMessage();
            require($rep . $vues['erreur']);
        }
    }
    public function waiting(){
        try {
            global $rep, $vues, $error;
            $model = new UserModel();
            $idPartie = $_SESSION['idPartie'];
            $etat = $model->getEtatPartie($idPartie);
            echo $etat;
        } catch (Exception $e) {
            $error = $e->getMessage();
            require($rep . $vues['erreur']);
        }
    }
    public function addToQueue(){
        try {
            global $rep, $vues, $error;
            $model = new UserModel();
            $utilisateur = $_SESSION['utilisateur'];
            if ($model->checkUserIsInPartie($utilisateur->getEmail())){
                throw new Exception("Vous êtes déjà dans une partie");
            }
            $idPartie = $model->addToQueue($utilisateur->getEmail());
            $_SESSION['idPartie'] = $idPartie;
            require($rep . $vues['queue']);
        } catch (Exception $e) {
            $error = $e->getMessage();
            require($rep . $vues['erreur']);
        }
    }

    public function goToGame()
    {
        try {
            global $rep, $vues, $error;
            $model = new UserModel();
            $utilisateur = $_SESSION['utilisateur'];
            $idPartie = $_GET['idPartie'];
            $index = $_GET['index'];
            $lastIndex = $model->getLastIndex($idPartie);
            if ($lastIndex != 0 && $index == $lastIndex + 1) {
                if ($model->checkGameIsEnd($idPartie)) {
                    $this->endGame();
                    return;
                }
                $points = $model->getPointsAtTheEnd($utilisateur->getEmail(), $idPartie);
                $dateDebut = $model->getDateDebut($idPartie);
                require($rep . $vues['lobbyEnd']);
            } else {
                if ($index == 1 ){
                    $model->majDateDebut($idPartie);
                }
                $enigme = $model->getEnigmebyPartieIdAndIndex($idPartie, $index);
                $model->resoudreEnigmeMulti($utilisateur, $enigme->getIdEnigme(), $idPartie, $index);
                $code = $model->getCode($utilisateur->getEmail(), $enigme->getIdEnigme());
                $dateDebut = $model->getDateDebut($idPartie);
                require($rep . $vues['partie']);
            }
        } catch (Exception $e) {
            $error = $e->getMessage();
            require($rep . $vues['erreur']);
        }
    }
    public function logout()
    {
        session_destroy();
        header('Location: index.php');
    }

    
    public function saveCode(){
        try {
            global $rep, $vues, $error;
            $model = new UserModel();
            $code = $_POST['code'];
            $ordre = $_POST['ordre'];
            $enigme = $model->getEnigmeByOrdre($ordre);
            $utilisateur=$_SESSION['utilisateur'];
            $model->saveCode($utilisateur->getEmail(),$enigme->getIdEnigme(),$code);
            echo $code;
        }
        catch (Exception $e) {
            $error = $e->getMessage();
            require($rep . $vues['erreur']);
        }
    }
    public function saveCodeMulti(){
        try {
            global $rep, $vues, $error;
            $model = new UserModel();
            $code = $_POST['code'];
            $index = $_POST['index'];
            $enigmeId = $_POST['enigmeId'];
            $utilisateur=$_SESSION['utilisateur'];
            $idPartie = $_SESSION['idPartie'];
            $model->saveCodeMulti($utilisateur->getEmail(),$enigmeId, $idPartie, $index,$code);
            echo $code;
        }
        catch (Exception $e) {
            $error = $e->getMessage();
            echo $error;
        }
    }
    public function saveCodeInCookie(){
        try {
            global $rep, $vues, $error;
            $model = new UserModel();
            $code = $_POST['code'];
            $num = $_POST['num'];
            setcookie("test".$num, $code, time() + 3600*24*365);
            echo $code;
        }
        catch (Exception $e) {
            $error = $e->getMessage();
            require($rep . $vues['erreur']);
        }
    }
    public function checkGameIsEnd(){
        try {
            global $rep, $vues, $error;
            $model = new UserModel();
            $idPartie = $_SESSION['idPartie'];
            $result = $model->checkGameIsEnd($idPartie);
            echo $result;
        }
        catch (Exception $e) {
            $error = $e->getMessage();
            require($rep . $vues['erreur']);
        }
    }

    public function getGameEtat(){
        try {
            global $rep, $vues, $error;
            $model = new UserModel();
            $idPartie = $_SESSION['idPartie'];
            $etat = $model->getEtatPartie($idPartie);
            echo $etat;
        }
        catch (Exception $e) {
            $error = $e->getMessage();
            require($rep . $vues['erreur']);
        }
    }
    
    public function endGame(){
        try {
            global $rep, $vues, $error;
            $model = new UserModel();
            $idPartie = $_SESSION['idPartie'];
            $lesInfos = $model->getEndGameInfo($idPartie);
            $model->endGame($idPartie);
            require ($rep . $vues['gameEnd']);
        }
        catch (Exception $e) {
            $error = $e->getMessage();
            require($rep . $vues['erreur']);
        }
    } 
    public function quitQueue(){
        try {
            global $rep, $vues, $error;
            $model = new UserModel();
            $idPartie = $_SESSION['idPartie'];
            $utilisateur = $_SESSION['utilisateur'];
            $model->quitQueue($utilisateur->getEmail(), $idPartie);
            require ($rep . $vues['lobby']);
        }
        catch (Exception $e) {
            $error = $e->getMessage();
            require($rep . $vues['erreur']);
        }
    } 
    public function quitGame(){
        try {
            global $rep, $vues, $error;
            $model = new UserModel();
            $idPartie = $_SESSION['idPartie'];
            $utilisateur = $_SESSION['utilisateur'];
            $model->quitGame($utilisateur->getEmail(), $idPartie);
            echo '<script>alert("Vous avez quitté la partie.");</script>';
            require ($rep . $vues['lobby']);
        }
        catch (Exception $e) {
            $error = $e->getMessage();
            require($rep . $vues['erreur']);
        }
    } 
    public function skipEnigme(){
        try {
            global $rep, $vues, $error;
            $model = new UserModel();
            $idPartie = $_SESSION['idPartie'];
            $utilisateur = $_SESSION['utilisateur'];
            $idEnigme = $_GET['id'];
            $index = $_GET['index'];
            $model->skipEnigme($utilisateur->getEmail(), $idPartie, $idEnigme);
            $index = $index + 1;
            header("Location: index.php?action=goToGame&idPartie=" . $idPartie . "&index=". $index);
        }
        catch (Exception $e) {
            $error = $e->getMessage();
            require($rep . $vues['erreur']);
        }
    }
    public function getPlayersPseudo(){
        try {
            global $rep, $vues, $error;
            $model = new UserModel();
            $idPartie = $_SESSION['idPartie'];
            $lesJoueurs = $model->getPlayersPseudo($idPartie);
            $lesJoueurs = json_encode($lesJoueurs);
            echo $lesJoueurs;
        }
        catch (Exception $e) {
            $error = $e->getMessage();
            require($rep . $vues['erreur']);
        }
    }
    public function getDashboardInfo(){
        try {
            global $rep, $vues, $error;
            $model = new UserModel();
            $idPartie = $_SESSION['idPartie'];
            $lesInfos = $model->getDashboardInfo($idPartie);
            $lesInfos = json_encode($lesInfos);
            echo $lesInfos;
        }
        catch (Exception $e) {
            $error = $e->getMessage();
            require($rep . $vues['erreur']);
        }
    }
}