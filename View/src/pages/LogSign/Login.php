﻿<!DOCTYPE html>
<html lang="fr">

<head>
  <meta charset="UTF-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <title>Login</title>
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/5.0.0-alpha1/css/bootstrap.min.css" integrity="sha384-r4NyP46KrjDleawBgD5tp8Y7UzmLA05oM1iAEQ17CSuDqnUK2+k9luXQOfXJCJ4I" crossorigin="anonymous" />
  <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/5.0.0-alpha1/js/bootstrap.min.js" integrity="sha384-oesi62hOLfzrys4LxRF63OJCXdXDipiYWBnvTl9Y9/TRlw5xlKIEHpNyvvDShgf/" crossorigin="anonymous"></script>
  <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Material+Symbols+Outlined:opsz,wght,FILL,GRAD@48,400,0,0" />
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css" />
  <link rel="stylesheet" href="./View/src/CSS/Login.css" />
  <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons" />
</head>

<body>
  <div class="container user-select-none">
    <div class="login-box col-12" id="form">
      <div class="row mb-4 d-flex align-items-center">
        <div class="col-2 justify-content-center">
          <a class="material-icons p-0 m-0" id="home" href="index.php?action=goToHome" style="font-size: 30px; color: white; text-decoration:none; width: fit-content;">home</a>
        </div>
        <div class="col d-flex justify-content-center align-content-center">
          <h2 class="text-uppercase font-weight-bold text-center" id="h2" style="width:fit-content;">Connexion</h2>
        </div>
        <div class="col-2"></div>
      </div>
      <form action="index.php?action=login" method="POST">
        <div class="user-box mb-5 position-relative">
          <input type="text" id="email" name="email" required="" />
          <label>Email</label>
        </div>
        <div class="user-box position-relative">
          <input type="password" id="password" name="password" required="" />
          <label>Mot de passe</label>
        </div>

        <div class="d-flex justify-content-around mt-5">
          <button class="py-2 px-3" type="submit">
            <span></span>
            <span></span>
            <span></span>
            <span></span>
            Envoyer
          </button>
          <a href="index.php?action=goToSignUp" class="right py-2 px-3 btn">
            <span></span>
            <span></span>
            <span></span>
            <span></span>
            Inscription
          </a>
        </div>
        <div class="text-center pt-4">
          <a href="index.php?action=goToMail" style="color : #5fc198; font-size:12px">Mots de passe oucliée ?</a>
        </div>
      </form>
    </div>
  </div>
</body>

</html>