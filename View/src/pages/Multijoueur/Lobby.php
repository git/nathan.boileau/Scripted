<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <title>Scripted</title>
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/5.0.0-alpha1/css/bootstrap.min.css" integrity="sha384-r4NyP46KrjDleawBgD5tp8Y7UzmLA05oM1iAEQ17CSuDqnUK2+k9luXQOfXJCJ4I" crossorigin="anonymous" />
  <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/5.0.0-alpha1/js/bootstrap.min.js" integrity="sha384-oesi62hOLfzrys4LxRF63OJCXdXDipiYWBnvTl9Y9/TRlw5xlKIEHpNyvvDShgf/" crossorigin="anonymous"></script>
  <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Material+Symbols+Outlined:opsz,wght,FILL,GRAD@48,400,0,0" />
  <link rel="stylesheet" href="./View/src/CSS/Main2.css" />
  <link rel="stylesheet" href="./View/src/CSS/FirstTest.css">
</head>

<body>
  <div id="imgMain" style="background: #050E15;">
    <div class="container-fluid">
      <div class="row m-0">
        <h2 class="text-center mt-3 pr-5" style="color: #00CCFF; font-weight: 1000; font-style: italic;">SCRIPT</h2>
        <h2 class="text-center pl-5" style="color: #D400D4; font-weight:bold; font-size:4em; font-style: italic;">RUSH</h2>
      </div>
      <div class="row pt-5 px-3 m-0">
        <p class="text-center">Bienvenue dans le mode <b>ScriptRush</b> ! Tu es ici dans le mode multijoueur de <b>Scripted</b>.
          Dans ce mode, tu vas pouvoir affronter d'autres joueurs et tenter de gagner le plus de points possible.
          Pour cela, tu vas devoir résoudre le plus d'énigme de possible en un temps imparti.
          Si c'est ta première venue, je te conseille de jeter un œil au mode <b>CodeQuest</b> avant d'aller plus loin.
          Pour les plus téméraires, je vous laisse cliquer sur <b>Lancer</b> pour trouver des adversaires.
          Bonne chance et que le meilleur gagne !</p>
      </div>
      <div class="row pt-5 m-0">
        <h3 class="text-center" id="waiting">Prêt ?</h3>
      </div>
      <div class="row m-0">
        <div class="d-flex justify-content-center ltext-center" style="cursor: pointer; height: 20%">
          <div class="m-3">
            <a class="btn" href="index.php?action=addToQueue">
              <span>LANCER</span>
            </a>
          </div>
          <div class="m-3">
            <a class="btn" href="index.php?action=goToHome">
              <span>RETOUR</span>
            </a>
          </div>
        </div>
      </div>
    </div>
  </div>
</body>

</html>