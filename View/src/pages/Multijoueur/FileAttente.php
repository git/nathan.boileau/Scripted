<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <title>Scripted</title>
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/5.0.0-alpha1/css/bootstrap.min.css" integrity="sha384-r4NyP46KrjDleawBgD5tp8Y7UzmLA05oM1iAEQ17CSuDqnUK2+k9luXQOfXJCJ4I" crossorigin="anonymous" />
  <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/5.0.0-alpha1/js/bootstrap.min.js" integrity="sha384-oesi62hOLfzrys4LxRF63OJCXdXDipiYWBnvTl9Y9/TRlw5xlKIEHpNyvvDShgf/" crossorigin="anonymous"></script>
  <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Material+Symbols+Outlined:opsz,wght,FILL,GRAD@48,400,0,0" />
  <link rel="stylesheet" href="./View/src/CSS/Main2.css" />
  <link rel="stylesheet" href="./View/src/CSS/FirstTest.css">
</head>

<body>
  <div id="imgMain" style="background: #050E15;">
    <div class="container-fluid">

      <div class="row p-3">
        <a class="btn" href="index.php?action=quitQueue">
          <span>Quitter</span>
        </a>
      </div>

      <div class="row m-0">
        <h2 class="text-center mt-3 pr-5" style="color: #00CCFF; font-weight: 1000;font-style: italic;">SCRIPT</h2>
        <h2 class="text-center pl-5" style="color: #D400D4; font-weight:bold ; font-size:4em;font-style: italic;">RUSH</h2>
      </div>

      <div class="row py-5 m-0">
        <h3 class="text-center" id="waiting">En attente de codeurs...</h3>
      </div>

      <div class="row py-5 d-flex m-0">
        <div class="col d-flex flex-column align-items-center">
          <div style="background-color: red; height: 75px; width: 75px;"></div>
          <p class="pt-3" id="player1">Joueur 1</p>
        </div>
        <div class="col d-flex flex-column align-items-center">
          <div style="background-color: blue; height: 75px; width: 75px;"></div>
          <p class="pt-3" id="player2">Joueur 2</p>
        </div>
        <div class="col d-flex flex-column align-items-center">
          <div style="background-color: yellow; height: 75px; width: 75px;"></div>
          <p class="pt-3" id="player3">Joueur 3</p>
        </div>
        <div class="col d-flex flex-column align-items-center">
          <div style="background-color: green; height: 75px; width: 75px;"></div>
          <p class="pt-3" id="player4">Joueur 4</p>
        </div>
      </div>

    </div>

  </div>
</body>

<script>
  function checkIsReady() {
    console.log("checkIsReady");
    var xhr = new XMLHttpRequest();
    xhr.open('POST', 'http://82.165.180.114/Scripted/index.php?action=waiting', true);
    xhr.responseType = 'text';
    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    xhr.onload = function() {
      console.log(xhr.responseText);
      if (xhr.responseText == "1") {
        window.location.href = 'http://82.165.180.114/Scripted/index.php?action=goToGame&idPartie=' + <?php echo $_SESSION['idPartie'] ?> + '&index=1'
      }
    };
    xhr.send(null);
  }

  function getPlayer() {
    console.log("getPlayer");
    var xhr = new XMLHttpRequest();
    xhr.open('POST', 'http://82.165.180.114/Scripted/index.php?action=getPlayersPseudo', true);
    xhr.responseType = 'text';
    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    xhr.onload = function() {
      var players = JSON.parse(xhr.responseText);
      if (players[0] != null) {
        document.getElementById("player1").innerHTML = players[0];
      }
      if (players[1] != null) {
        document.getElementById("player2").innerHTML = players[1];
      }
      if (players[2] != null) {
        document.getElementById("player3").innerHTML = players[2];
      }
      if (players[3] != null) {
        document.getElementById("player4").innerHTML = players[3];
      }
    };
    xhr.send(null);
  }
  window.addEventListener("load", waiting);

  function waiting() {
    var countdownTimer = setInterval('checkIsReady()', 1000);
    setInterval('getPlayer()', 500);
  }
</script>

</html>