<!doctype html>
<html lang="en">

<head>
  <title>Scripted</title>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <!-- Bootstrap CSS v5.2.1 -->
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/css/bootstrap.min.css" rel="stylesheet"
    integrity="sha384-iYQeCzEYFbKjA/T2uDLTpkwGzCiq6soy8tYaI1GyVh/UjpbCx/TYkiZhlZB6+fzT" crossorigin="anonymous">
  <link rel="stylesheet" href="./View/src/CSS/GameEnd.css" />
</head>

<body class="m-5">
  <a name="" id="" class="btn" href="index.php?action=goToHome" role="button">Retour</a>
  <div class="d-flex flex-column align-items-center">
    <h1>Partie Terminée !</h1>
    <h2>Gagnant : <?php echo $lesInfos[3]->getPseudo() ?></h2>
    <p>Bien joué à tous la partie est fini, voici le classement :</p>
  </div>
  <table class="table">
    <thead class="text-center">
      <tr>
        <th scope="col">Classement</th>
        <th scope="col">Nom</th>
        <th scope="col">Point</th>
        <th scope="col">Temps (en secondes)</th>
      </tr>
    </thead>
    <tbody>
      <?php
      global $playerNumberPerGame;
      $i = 0;
      for ($i; $i < $playerNumberPerGame; $i++) {
        echo '<tr class="text-center">';
        echo '<th scope="row"> ' . ($i+1) . '</th>';
        echo "<td>" . $lesInfos[0][$i]->getPseudo() . "</td>";
        echo "<td>" . $lesInfos[1][$i] . "</td>";
        echo "<td>" . $lesInfos[2][$i] . "</td>";
        echo "</tr>";
      }
      ?>
    </tbody>
    <!-- Bootstrap JavaScript Libraries -->
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.11.6/dist/umd/popper.min.js"
      integrity="sha384-oBqDVmMz9ATKxIep9tiCxS/Z9fNfEXiDAYTujMAeBAsjFuCZSmKbSSUnQlmh/jp3" crossorigin="anonymous">
      </script>

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/js/bootstrap.min.js"
      integrity="sha384-7VPbUDkoPSGFnVtYi0QogXtr74QeVeeIs99Qfg5YCF+TidwNdjvaKZX19NZ/e6oz" crossorigin="anonymous">
      </script>
</body>

</html>