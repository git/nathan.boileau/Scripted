<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <title>
    <?php
      echo $enigme->getNom();
    ?>
  </title>
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/5.0.0-alpha1/css/bootstrap.min.css"
    integrity="sha384-r4NyP46KrjDleawBgD5tp8Y7UzmLA05oM1iAEQ17CSuDqnUK2+k9luXQOfXJCJ4I" crossorigin="anonymous" />
  <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
    integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo"
    crossorigin="anonymous"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/5.0.0-alpha1/js/bootstrap.min.js"
    integrity="sha384-oesi62hOLfzrys4LxRF63OJCXdXDipiYWBnvTl9Y9/TRlw5xlKIEHpNyvvDShgf/"
    crossorigin="anonymous"></script>
  <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons" />
  <link rel="stylesheet" href="View/src/CSS/Enigme.css" />
</head>

<body>
  <div class="container-fluid px-5">
    <!-- First Row -->
    <div class="row py-4 d-flex justify-content-between user-select-none">
      <div class="col d-flex align-items-center px-0">
        <a class="material-icons pl-0" id="home" href="index.php?action=quitGame"
          style="font-size: 40px; color: white">home</a>
      </div>
      
      <div class="col-2 d-flex align-items-center justify-content-center px-2">
        <a href=<?php echo '"index.php?action=skipEnigme&id='. $enigme->getIdEnigme() . '&index='. $index .'"' ?> class="px-3 py-2"><b>Passer l'énigme</b></a>
      </div>

      <div class="col-5 d-flex align-items-center justify-content-center px-0">
        <p>Temps restant : &nbsp;</p>
        <?php
          $end_time = $dateDebut->modify('+'. $_SESSION['tpsMaxPartie'] .'seconds');
          $now = new DateTime();
          $interval = $now->diff($end_time);
          $remaining_seconds = $interval->days * 24 * 60 * 60 + $interval->h * 60 * 60 + $interval->i * 60 + $interval->s;
        ?>
        <div id="countdown"></div>
      </div>
      <button style="background-color: transparent; border: none; outline: none;" onclick="displayHelp()"
        class="col d-flex align-items-center">
        <div class="col"></div>
        <div class="col-3 d-flex justify-content-end px-2" style="width:fit-content">
          <p class="mr-3" style="font-size: 16px; font-family: Equinox; color: white;"><b>Besoin d'aide ?</b></p>
        </div>
        <div class="col-1 text-right">
          <img src="View/assets/img/Foxy.png" alt="Logo" class="rounded-circle moving-fox"
            style="border: 1px solid #44fff6; width: 60px; height: 60px" />
        </div>
      </button>
    </div>
    <!-- End First Row -->

    <!-- Second Row -->
    <div class="row">
      <!-- First Column -->
      <div class="col-3 rounded p-3" style="background-color: #222831; min-height: 80vh; height: auto">
        <h2 class="text-left py-3" style="color: #44fff6; font-weight: 500">
          <?php
          echo $enigme->getNom();
          ?>
        </h2>
        <p>
          <?php
          echo $enigme->getEnonce();
          ?>
        </p>
        <h3 class="text-left pb-3 pt-5" style="color: #44fff6">Rappel</h3>
        <p>
          <?php
          echo $enigme->getRappel();
          ?>
        </p>
        <h3 class="text-left pb-3 pt-5" style="color: #44fff6">Exemple</h3>
        <p>
          <?php
          echo $enigme->getExemple();
          ?>
        </p>
        <h3 class="text-left pb-3 pt-5 help" style="color: #44fff6; display: none">Aide</h3>
        <p style="display: none" class="help">
          <?php
          echo $enigme->getAide();
          ?>
        </p>
      </div>
      <!-- End First Column -->

      <!-- Second Column -->
      <div class="col-5 pr-0">
        <div class="ace rounded" id="editor"><?php 
        // echo $code;
        if (! isset($code) || $code == ""){
          echo $enigme->getPrompt();
        }
        else {
          echo $code;}
        ?></div>
      </div>
      <!-- End Second Column -->

      <!-- Third Column -->
      <div class="col-4">
        <textarea id="console" readonly style="width: 100%; min-height: 65vh; height: auto"
          class="p-3 rounded"></textarea>

        <div class="row pt-5 text-center" style="cursor: pointer">
          <div class="col-4">
            <a onclick="run_init()" class="btn">
              <span>Exécuter</span>
            </a>
          </div>
          <div class="col-4">
            <button onclick="submit()" class="btn" data-toggle="modal" data-target="#modal">
              <span>Envoyer</span>
            </button>
          </div>
          <div class="col-4">
            <a onclick="dashboard()" class="btn">
              <span>Dashboard</span>
            </a>
          </div>
        </div>
      </div>
      <!-- End Third Column -->
    </div>
    <!-- End Second Row -->
  </div>

  <!-- Modal -->
  <div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
    aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h2 class="modal-title" id="exampleModalLongTitle" style="color: black">
            Resultat
          </h2>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <h5 id="result" style="color: black"></h5>
        </div>
        <div class="modal-footer">
          <a
          <?php
          echo 'href="index.php?action=enigmeMultiEnded&index=' . $index .'"';
          ?>
          class="btn" style="display: none" id="next"
          >
            <span>SUIVANT</span>
          </a>
        </div>
      </div>
    </div>
  </div>
  <!-- End Modal -->

  <script src="https://ajaxorg.github.io/ace-builds/src-noconflict/ace.js" type="text/javascript"
    charset="utf-8"></script>
  <script src="https://raw.githack.com/pythonpad/brython-runner/master/lib/brython-runner.bundle.js"
    type="text/javascript" charset="utf-8"></script>
  <script src="View/src/JS/baseMulti.js">
  </script>
  <script> 
    // const URL = "http://localhost/Scripted/index.php";
  // const URL = "http://82.165.180.114/Scripted/index.php";
  const URL = "http://sancy.iut.uca.fr/~lafourcade/Scripted/index.php";
    function saveCode(index, enigmeId) {
      console.log("saveCode");
      var index = '<?php echo $index; ?>';
      var enigmeId = '<?php echo $enigme->getIdEnigme(); ?>';
      console.log("[Savecode]index : " + index, "[Savecode]enigmeId : " + enigmeId);
      var xhr = new XMLHttpRequest();
      xhr.open('POST', URL + '?action=saveCodeMulti', true);
      xhr.responseType = 'text';
      xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
      xhr.onload = function () {
        console.log('[SaveCode] '+xhr.responseText);
      };
      var searchParams = new URLSearchParams(window.location.search);
      xhr.send("code=" + editor.getValue() + "&index=" + index + "&enigmeId=" + enigmeId);
  }
  document.getElementById ('editor').addEventListener('input', saveCode);

  var seconds = <?php echo $remaining_seconds; ?>;
    function countdown() {;
      var days        = Math.floor(seconds/24/60/60);
      var hoursLeft   = Math.floor((seconds) - (days*86400));
      var hours       = Math.floor(hoursLeft/3600);
      var minutesLeft = Math.floor((hoursLeft) - (hours*3600));
      var minutes     = Math.floor(minutesLeft/60);
      var remainingSeconds = seconds % 60;
      if (remainingSeconds < 10) {
          remainingSeconds = "0" + remainingSeconds; 
      }
      if (hours == 0){
        document.getElementById('countdown').innerHTML = minutes + " minutes " + remainingSeconds + " secondes";
      }
      else if (minutes == 0){
        document.getElementById('countdown').innerHTML = "Il reste moins d'une minute.";
      }
      else {
        document.getElementById('countdown').innerHTML = hours + " heures " + minutes + " minutes " + remainingSeconds + " secondes";
      }
      if (seconds == 0) {
        clearInterval(countdown);
        window.location.href = 'http://82.165.180.114/Scripted/index.php?action=endGame'
      } else {
          seconds--;
      }
    }
    var countdownTimer = setInterval('countdown()', 1000);
  </script>
  <script src="View/src/JS/Submit.js"></script>
  <?php
  echo '<script>
    let solution = `'. $enigme->getSolution() .'`;
    let test = `'. $enigme->getTest() .'`;
    let btnSubmit = document.querySelector("#submit");
    btnSubmit.addEventListener("click", function(){
        submit(solution, test);
    });
  </script>';
  ?>
</body>

</html>