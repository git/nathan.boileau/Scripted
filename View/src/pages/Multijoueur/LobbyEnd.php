<!doctype html>
<html lang="en">

<head>
    <title>Scripted</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-iYQeCzEYFbKjA/T2uDLTpkwGzCiq6soy8tYaI1GyVh/UjpbCx/TYkiZhlZB6+fzT" crossorigin="anonymous">
    <link rel="stylesheet" href="./View/src/CSS/LobbyEnd.css" />
</head>

<body>
    <div class="container mt-5 p-3" id="bordered">
        <div class="row">
            <div class="col-12 d-flex justify-content-center">
                <h1>Salon de fin de partie</h1>
            </div>
        </div>
        <div class="row">
            <div class="col-12 d-flex flex-column align-items-center">
                <p>Vous avez terminé toutes les énigmes.</p>
                <p>Votre score est de : <?php echo $points ?> points.</p>
                <p>Temps restant avant la fin de la partie :</p>
                <?php
                $end_time = $dateDebut->modify('+' . $_SESSION['tpsMaxPartie'] . 'seconds');
                $now = new DateTime();
                $interval = $now->diff($end_time);
                $remaining_seconds = $interval->days * 24 * 60 * 60 + $interval->h * 60 * 60 + $interval->i * 60 + $interval->s;
                ?>
                <div id="countdown"></div>
            </div>
        </div>
    </div>
    <script>
        function checkIsEnd() {
            var xhr = new XMLHttpRequest();
            xhr.open('POST', 'http://82.165.180.114/Scripted/index.php?action=getGameEtat', true);
            xhr.responseType = 'text';
            xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
            xhr.onload = function() {
                if (xhr.responseText == "2") {
                    window.location.href = 'http://82.165.180.114/Scripted/index.php?action=endGame'
                }
            };
            xhr.send(null);
        }
        var seconds = <?php echo $remaining_seconds; ?>;

        function countdown() {
            checkIsEnd();
            var days = Math.floor(seconds / 24 / 60 / 60);
            var hoursLeft = Math.floor((seconds) - (days * 86400));
            var hours = Math.floor(hoursLeft / 3600);
            var minutesLeft = Math.floor((hoursLeft) - (hours * 3600));
            var minutes = Math.floor(minutesLeft / 60);
            var remainingSeconds = seconds % 60;
            if (remainingSeconds < 10) {
                remainingSeconds = "0" + remainingSeconds;
            }
            if (hours == 0){
                document.getElementById('countdown').innerHTML = minutes + " minutes " + remainingSeconds + " secondes";
            }
            else if (minutes == 0){
                document.getElementById('countdown').innerHTML = "Il reste moins d'une minute.";
            }
            else {
                document.getElementById('countdown').innerHTML = hours + " heures " + minutes + " minutes " + remainingSeconds + " secondes";
            }
            if (seconds == 0) {
                clearInterval(countdown);
                window.location.href = 'http://82.165.180.114/Scripted/index.php?action=endGame'
            } else {
                seconds--;
            }
        }
        var countdownTimer = setInterval('countdown()', 1000);
    </script>
    <!-- Bootstrap JavaScript Libraries -->
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.11.6/dist/umd/popper.min.js" integrity="sha384-oBqDVmMz9ATKxIep9tiCxS/Z9fNfEXiDAYTujMAeBAsjFuCZSmKbSSUnQlmh/jp3" crossorigin="anonymous">
    </script>

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/js/bootstrap.min.js" integrity="sha384-7VPbUDkoPSGFnVtYi0QogXtr74QeVeeIs99Qfg5YCF+TidwNdjvaKZX19NZ/e6oz" crossorigin="anonymous">
    </script>
</body>

</html>