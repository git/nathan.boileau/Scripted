<!DOCTYPE html>
<html lang="en">
<head>
    <title>Admin</title>
    <!-- Required meta tags -->
    <meta charset="utf-8" />
    <meta
      name="viewport"
      content="width=device-width, initial-scale=1, shrink-to-fit=no"
    />

    <!-- CSS -->
    <link
      href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/css/bootstrap.min.css"
      rel="stylesheet"
      integrity="sha384-F3w7mX95PdgyTmZZMECAngseQB83DfGTowi0iMjiWaeVhAn4FJkqJByhZMI3AhiU"
      crossorigin="anonymous"
    />
    <link rel="stylesheet" href="./View/src/CSS/Admin2.css" />
</head>
<body class="m-3">
    <div>
        <a href="index.php?action=goToAdmin" class="send py-1 px-2 d-flex align-items-center">
            Retour
        </a>
        <h1> Modifier une énigme </h1>
    </div>
    <form 
    <?php echo 'action="index.php?action=editEnigme&id='. $enigme->getIdEnigme() .'"'; ?>
    method="POST">
        <div class="col">
            <div class="mb-3">
                <label for="" class="form-label">Nom</label>
                <textarea class="form-control" name="nom" id="nom" rows="3" required><?php echo $enigme->getNom(); ?></textarea>
            </div>
        </div>
        <div class="col">
            <div class="mb-3">
                <label for="" class="form-label">Enigme</label>
                <textarea class="form-control" name="enigme" id="enigme" rows="3" required><?php echo $enigme->getEnonce(); ?></textarea>
            </div>
        </div>
        <div class="col">
            <div class="mb-3">
                <label for="" class="form-label">Rappel</label>
                <textarea class="form-control" name="rappel" id="rappel" rows="3"><?php echo $enigme->getRappel(); ?></textarea>
            </div>
        </div>
        <div class="col">
            <div class="mb-3">
                <label for="" class="form-label">Aide</label>
                <textarea class="form-control" name="aide" id="aide" rows="3"><?php echo $enigme->getAide(); ?></textarea>
            </div>
        </div>
        <div class="col">
            <div class="mb-3">
                <label for="" class="form-label">Exemple</label>
                <textarea class="form-control" name="exemple" id="exemple" rows="3"> <?php echo $enigme->getExemple(); ?></textarea>
            </div>
        </div>
        <div class="col">
            <div class="mb-3">
                <label for="" class="form-label">Solution</label>
                <textarea class="form-control" name="solution" id="solution" rows="3" required><?php echo $enigme->getSolution(); ?></textarea>
            </div>
        </div>
        <div class="col">
            <div class="mb-3">
                <label for="" class="form-label">Test</label>
                <textarea class="form-control" name="test" id="test" rows="3" required> <?php echo $enigme->getTest(); ?></textarea>
            </div>
        </div>
        <div class="col">
            <div class="mb-3">
                <label for="" class="form-label">Prompt, ce qu'il sera écrit par défaut dans l'éditeur</label>
                <textarea class="form-control" name="prompt" id="prompt" rows="3"><?php echo $enigme->getPrompt(); ?></textarea>
            </div>
        </div>
        <button class="left send py-1 px-2 m-2 d-flex align-items-center" type="submit">
            Modifier
        </button>
    </form>
        <div class="d-flex align-items-center mt-3">
            <button onclick="submit()" class="left send py-1 px-2 mx-2 d-flex align-items-center">
                Verfier la solution
            </button>
            <a class="left send py-1 px-2 mx-2 d-flex align-items-center" href="<?php echo 'index.php?action=goToEnigme&ordre='. $enigme->getOrdre();?>">
                Se rendre à l'énigme
            </a>
            <p id="result"></p>
        </div>
    <script src="https://raw.githack.com/pythonpad/brython-runner/master/lib/brython-runner.bundle.js"
        type="text/javascript" charset="utf-8"></script>
    <script src="View/src/JS/Admin2.js"></script>
    </body>
</html>
   