<!DOCTYPE html>
<html lang="en">
<head>
    <title>Admin</title>
    <!-- Required meta tags -->
    <meta charset="utf-8" />
    <meta
      name="viewport"
      content="width=device-width, initial-scale=1, shrink-to-fit=no"
    />

    <!-- CSS -->
    <link
      href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/css/bootstrap.min.css"
      rel="stylesheet"
      integrity="sha384-F3w7mX95PdgyTmZZMECAngseQB83DfGTowi0iMjiWaeVhAn4FJkqJByhZMI3AhiU"
      crossorigin="anonymous"
    />
    <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Material+Symbols+Outlined:opsz,wght,FILL,GRAD@20..48,100..700,0..1,-50..200" />
    <link rel="stylesheet" href="./View/src/CSS/Admin2.css" />
</head>
<body>
    <div class="container">
        <div class="row">
            <div class="col">
                <a href="index.php?action=goToAdmin" class="send mt-3 mb-5 px-3 py-2 d-flex align-items-center">
                    Retour
                </a>
                <h1>Ordre des énigmes</h1>
            </div>
        </div>
        <div class="row">
            <div class="col">
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th scope="col">Enigme</th>
                            <th scope="col-4">Ordre</th>
                            <!-- <th scope="col"></th> -->
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                            foreach ($lesEnigmes as $enigme) {
                                echo "<tr>";
                                echo '<td> <a href=index.php?action=modifOrdre&id='. $enigme->getIdEnigme() .'&dir=up class="material-symbols-outlined mx-2">arrow_upward </a>';
                                echo '<a href=index.php?action=modifOrdre&id='. $enigme->getIdEnigme() .'&dir=down class="material-symbols-outlined mx-2">arrow_downward </a>';
                                echo $enigme->getNom();
                                echo '</td>';
                                echo "<td>" . $enigme->getOrdre() . "</td>";
                                echo "</tr>";
                            }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</body>