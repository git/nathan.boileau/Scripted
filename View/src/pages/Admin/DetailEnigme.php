<!DOCTYPE html>
<html lang="en">

<head>
    <title>Admin</title>
    <!-- Required meta tags -->
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />

    <!-- CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-F3w7mX95PdgyTmZZMECAngseQB83DfGTowi0iMjiWaeVhAn4FJkqJByhZMI3AhiU" crossorigin="anonymous" />
    <link rel="stylesheet" href="./View/src/CSS/Admin2.css" />
</head>

<body>
    <div class="container">
        <div class="row">
            <div class="col">
                <a href="index.php?action=goToAdmin" class="send mt-3 mb-5 px-3 py-2 d-flex align-items-center">Retour</a>
                <h1>Enigme</h1>
            </div>
        </div>
        <div class="row">
            <div class="col">
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th scope="col">Champ</th>
                            <th scope="col">Donnée</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        if ($enigme->getOrdre() != 0) {
                            echo '<tr>';
                            echo '<td>Ordre</td>';
                            echo "<td>" . $enigme->getOrdre() . "</td>";
                            echo '</tr>';
                        }
                        ?>
                        <tr>
                            <td>Nom</td>
                            <?php echo "<td>" . $enigme->getNom() . "</td>"; ?>
                        </tr>
                        <tr>
                            <td>Enonce</td>
                            <?php echo "<td>" . $enigme->getEnonce() . "</td>"; ?>
                        </tr>
                        <tr>
                            <td>Aide</td>
                            <?php echo "<td>" . $enigme->getAide() . "</td>"; ?>
                        </tr>
                        <tr>
                            <td>Rappel</td>
                            <?php echo "<td>" . $enigme->getRappel() . "</td>"; ?>
                        </tr>
                        <tr>
                            <td>Exemple</td>
                            <?php echo "<td>" . $enigme->getExemple() . "</td>"; ?>
                        </tr>
                        <tr>
                            <td>Solution</td>
                            <?php echo "<td>" . $enigme->getSolution() . "</td>"; ?>
                        </tr>
                        <tr>
                            <td>Test</td>
                            <?php echo "<td>" . $enigme->getTest() . "</td>"; ?>
                        </tr>
                        <tr>
                            <td>Prompt</td>
                            <?php echo "<td>" . $enigme->getPrompt() . "</td>"; ?>
                        </tr>
                        <?php
                        if ($enigme->getPoints() != 0 && $enigme->getTempsDeResolution() != 0) {
                            echo '<tr>';
                            echo '<td>Points</td>';
                            echo "<td>" . $enigme->getPoints() . "</td>";
                            echo '</tr>';
                            echo '<tr>';
                            echo '<td>Temps de résolution</td>';
                            echo "<td>" . $enigme->getTempsDeResolution() . "</td>";
                            echo '</tr>';
                        }
                        ?>
                        <tr>
                            <td>Modifier</td>
                            <?php
                            if ($enigme->getOrdre() != 0) {
                                echo '<td><a class="link" href="index.php?action=goToModifEnigmeSolo&id=' . $enigme->getIdEnigme() . '">Modifier</a></td>';
                            } else {
                                echo '<td><a class="link" href="index.php?action=goToModifEnigmeMulti&id=' . $enigme->getIdEnigme() . '"\">Modifier</a></td>';
                            }
                            ?>
                        </tr>
                        <tr>
                            <td>Supprimer</td>
                            <?php echo '<td><a class="link" href="index.php?action=deleteEnigme&id=' . $enigme->getIdEnigme() . '">Supprimer</a></td>' ?>
                        </tr>
                        <tr>
                            <?php
                            if ($enigme->getPoints() == 0 && $enigme->getTempsDeResolution() == 0) {
                                echo '<td>Tester </td>';
                                echo '<td><a class="link" href="index.php?action=goToEnigme&ordre=' . $enigme->getOrdre() . '">Tester</a></td>';
                            }
                            ?>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</body>