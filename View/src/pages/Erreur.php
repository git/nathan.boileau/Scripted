<!DOCTYPE html>
<html lang="en">
  <head>
    <title>Scripted</title>
    <meta charset="utf-8" />
    <meta
      name="viewport"
      content="width=device-width, initial-scale=1, shrink-to-fit=no"
    />
    <link rel="stylesheet" href="./View/src/CSS/Erreur.css" />

  </head>
<body>
<div class="noise"></div>
<div class="overlay"></div>
<div class="terminal">
<?php
if ($error == 400){
  echo '<h1>Erreur <span class="errorcode">404</span></h1>';
  echo '<p class="output">La page que vous recherchez a peut-être été supprimée, a changé de nom ou est temporairement indisponible.</p>';
  echo '<p class="output">Veuillez essayer de <a href="javascript:history.back()">retourner en arrière</a> ou de <a href="index.php?action=goToHome">retourné à la page d\'accueil</a>.</p>';
  echo '<p class="output">Bonne Chance Utilisateur.</p>';
}
elseif ($error == ""){
  echo '<h1>Erreur <span class="errorcode">000</span></h1>';
  echo '<p class="output">Erreur inconnue</p>';
  echo '<p class="output">Veuillez essayer de <a href="javascript:history.back()">retourner en arrière</a> ou de <a href="index.php?action=goToHome">retourné à la page d\'accueil</a>.</p>';
  echo '<p class="output">Bonne Chance Utilisateur.</p>';
}

else{
  echo '<h1>Erreur <span class="errorcode">000</span></h1>';
  echo '<p class="output">'.$error.'</p>';
  echo '<p class="output">Veuillez essayer de <a href="javascript:history.back()">retourner en arrière</a> ou de <a href="index.php?action=goToHome">retourné à la page d\'accueil</a>.</p>';
  echo '<p class="output">Bonne Chance Utilisateur.</p>'; 
}
?>
</div>
</body>
</html>
