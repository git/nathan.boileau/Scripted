<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Test de qualification</title>
    <link
      rel="stylesheet"
      href="https://stackpath.bootstrapcdn.com/bootstrap/5.0.0-alpha1/css/bootstrap.min.css"
      integrity="sha384-r4NyP46KrjDleawBgD5tp8Y7UzmLA05oM1iAEQ17CSuDqnUK2+k9luXQOfXJCJ4I"
      crossorigin="anonymous"
    />
    <script
      src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
      integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo"
      crossorigin="anonymous"
    ></script>
    <script
      src="https://stackpath.bootstrapcdn.com/bootstrap/5.0.0-alpha1/js/bootstrap.min.js"
      integrity="sha384-oesi62hOLfzrys4LxRF63OJCXdXDipiYWBnvTl9Y9/TRlw5xlKIEHpNyvvDShgf/"
      crossorigin="anonymous"
    ></script>
    <link rel="stylesheet" href="View/src/CSS/FirstTest.css" />
  </head>
  <body>
    <nav class="navbar navbar-expand-lg navbar-dark pb-5">
      <div class="container-fluid mx-0">
        <div class="nav-item nav-link">
          <a class="navbar-brand" href="index.php?action=goToHome">Accueil</a>
        </div>
        <div class="mx-auto d-flex">
          <h5
            class="m-1 text-uppercase"
            style="color: #fff; font-weight: bold; font-size: 22px"
          >
          Test de qualification
          </h5>
          <h5
          class="m-1 text-uppercase"
          style="color: #44fff6; font-weight: bold; font-size: 22px"
        > 
          3/10
        </h5>
        </div>
        <div class="nav-link">
          <a class="navbar-brand" href="index.php?action=goToNext&num=2">Précédent</a>
        </div>
        <div class="nav-link">
          <a class="navbar-brand" href="index.php?action=goToNext&num=4">Suivant</a>
        </div>
      </div>
    </nav>

    <div class="container">
      <!-- First Test -->
      <div
        class="row rounded p-3 m-3"
        style="
          background: #16222a; /* fallback for old browsers */
          background: -webkit-linear-gradient(
            to right,
            #3a6073,
            #16222a
          ); /* Chrome 10-25, Safari 5.1-6 */
          background: linear-gradient(to right, #3a6073, #16222a);
        "
      >
        <div class="row">
          <div class="col-10">
            <section
              style="background-color: #222831; min-height: 0"
              class="p-3 rounded m-0"
            >
              <p>
                Passons désormais à la suite de notre test de qualification. Je vais te présenter les <b style="color:violet">listes</b>.
              </p>
              <p>
                Une liste est une structure de données qui permet de stocker plusieurs valeurs. En python, on peut créer une liste en utilisant les crochets <b style="color:violet">[ ]</b>.
              </p>
              <p>
                Voici un exemple :
              </p>
              <code style="font-size: 18px">
                ma_liste = [1, 2,"Hello","World", 3.14]
              </code>
              <br><br>
              <p>
                Ici, on crée une liste composée de plusieurs éléments. On peut accéder à un élément de la liste en utilisant son <b style="color:violet">indice</b>. L'indice d'un élément correspond à sa position dans la liste. On commence à compter à partir de 0.
              </p>
              <code style="font-size: 18px">
                ma_liste[0] = 3
              </code>
              <br><br>
              <p>
                Dans l'exemple précédent, dans la liste <b style="color:violet">ma_liste</b>, on accède à l'élément d'indice 0 et on lui donne la valeur 3.
              </p>
              <p>
                Désormais, c'est à toi d'expérimenter ! Je t'ai préparé un petit
                programme qui va t'en apprendre plus sur les listes. Si tu veux
                plus d'information n'hésite pas à aller voir sur ce site : <a href="https://docs.python.org/fr/3/tutorial/datastructures.html" target="_blank" style="color: #44fff6">https://docs.python.org/fr/3/tutorial/datastructures.html</a>
              </p>
            </section>
          </div>
          <div class="col-2 align-self-center">
            <img
              src="View/assets/img/Foxy.png"
              alt="Logo"
              class="img-fluid rounded-circle"
              style="
                border: 2px solid #44fff6;
                background-image: url('View/assets/img/BackgroundMain.jpg');
                background-size: cover;
                background-position: center;
              "
            />
          </div>
        </div>
        <div class="row mt-5">
          <!-- Editor -->
          <div class="col-8">
            <div class="ace rounded ace-1" id="editor" style="min-height: 40vh"><?php
            if (isset ($_COOKIE['test3'])) {
                echo $_COOKIE['test3'];
            }
            else {
                echo '# Initialise une liste
ma_liste = [1, 2,"Hello","World", 3.14]
print("Notre liste de départ :")
print(ma_liste)
print("")

# Remplace le premier élément par 3
print("Remplacement du premier élément par 3")
ma_liste[0] = 3
print(ma_liste)
print("")

# Remplace le dernier élément par "toto"
print("Remplacement du dernier élément par \'toto\'")
ma_liste[-1] = "toto"
print(ma_liste)
print("")

# Ajoute 5.4 a la fin de la liste
print("Ajout de 5.4 a la fin de la liste")
ma_liste.append(5.4)
print(ma_liste)
print("")

# Insert 2 à l\'index 3
print("Insertion de 2 à l\'index 3")
ma_liste.insert(3,2)
print(ma_liste)
print("")

# Supprimer le premier 2 de la liste
print("Suppretion du premier 2 de la liste")
ma_liste.remove(2)
print(ma_liste)
print("")

# Enlève de la liste l\'élément situé à la position 1 et le renvoie
print("Suppression de l\'élément situé à la position 1")
p = ma_liste.pop(1)
print(ma_liste)
print("L\'élément retiré :", p)
print("")

# Renvoie la longueur de la liste
print("Longueur de la liste :", len(ma_liste))
print("")

# Ps l\'instruction \'len\' ne fonctionne pas que pour les listes
print("Longueur de la chaine de caractère \'toto\' :", len("toto"))';
            }
          ?></div>
          </div>
          <!-- End Editor -->

          <!-- Console -->
          <div class="col-4" style="min-height: 40vh">
            <textarea
              id="console"
              readonly
              style="width: 100%; height: 60%"
              class="p-3 rounded"
            ></textarea>
            <!-- End Return Code -->

            <!-- Buttons -->
            <div
              class="row pt-5 text-center"
              style="cursor: pointer; height: 20%"
            >
              <div class="col">
                <a onclick="run_init()" class="btn">
                  <span>Exécuter</span>
                </a>
              </div>
            </div>
            <!-- End Buttons -->
          </div>
          <!-- End Console -->
        </div>
      </div>
      <!-- End First Test -->
    </div>

    <script
      src="https://ajaxorg.github.io/ace-builds/src-noconflict/ace.js"
      type="text/javascript"
      charset="utf-8"
    ></script>
    <script
      src="https://raw.githack.com/pythonpad/brython-runner/master/lib/brython-runner.bundle.js"
      type="text/javascript"
      charset="utf-8"
    ></script>
    <script src="View/src/JS/baseTest.js"></script>
  </body>
</html>