﻿<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Test de qualification</title>
    <link
      rel="stylesheet"
      href="https://stackpath.bootstrapcdn.com/bootstrap/5.0.0-alpha1/css/bootstrap.min.css"
      integrity="sha384-r4NyP46KrjDleawBgD5tp8Y7UzmLA05oM1iAEQ17CSuDqnUK2+k9luXQOfXJCJ4I"
      crossorigin="anonymous"
    />
    <script
      src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
      integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo"
      crossorigin="anonymous"
    ></script>
    <script
      src="https://stackpath.bootstrapcdn.com/bootstrap/5.0.0-alpha1/js/bootstrap.min.js"
      integrity="sha384-oesi62hOLfzrys4LxRF63OJCXdXDipiYWBnvTl9Y9/TRlw5xlKIEHpNyvvDShgf/"
      crossorigin="anonymous"
    ></script>
    <link rel="stylesheet" href="View/src/CSS/FirstTest.css" />
  </head>
  <body>
    <nav class="navbar navbar-expand-lg navbar-dark pb-5">
      <div class="container-fluid mx-0">
        <div class="nav-item nav-link">
          <a class="navbar-brand" href="index.php?action=goToHome">Accueil</a>
        </div>
        <div class="mx-auto d-flex">
          <h5
            class="m-1 text-uppercase"
            style="color: #fff; font-weight: bold; font-size: 22px"
          >
          Test de qualification
          </h5>
          <h5
          class="m-1 text-uppercase"
          style="color: #44fff6; font-weight: bold; font-size: 22px"
        > 
          2/10
        </h5>
        </div>
        <div class="nav-link">
          <a class="navbar-brand" href="index.php?action=goToTest">Précédent</a>
        </div>
        <div class="nav-link">
          <a class="navbar-brand" href="index.php?action=goToNext&num=3">Suivant</a>
        </div>
      </div>
    </nav>

    <div class="container">
      <!-- First Test -->
      <div
        class="row rounded p-3 m-3"
        style="
          background: #16222a; /* fallback for old browsers */
          background: -webkit-linear-gradient(
            to right,
            #3a6073,
            #16222a
          ); /* Chrome 10-25, Safari 5.1-6 */
          background: linear-gradient(to right, #3a6073, #16222a);
        "
      >
        <div class="row">
          <div class="col-10">
            <section
              style="background-color: #222831; min-height: 0"
              class="p-3 rounded m-0"
            >
              <p>
                Pour ce deuxième test, nous allons voir comment déclarer une
                variable et la manipuler. Pour créer une variable en python, il
                suffit de faire :
              </p>
              <code style="font-size: 18px">x = 1</code>
              <br /><br />
              <p>
                Ici, nous avons créé une variable x qui contient la valeur 1.
                Nous pouvons maintenant manipuler cette variable en faisant :
              </p>
              <code style="font-size: 18px">x = x + 1</code>
              <br /><br />
              <p>
                Ici, nous avons incrémenté la valeur de x de 1. Nous pouvons
                également faire toute autre opération mathématique avec cette
                même variable. Par exemple :
              </p>
              <code style="font-size: 18px">x = x * 2</code><br />
              <code style="font-size: 18px">x = x / 2</code><br />
              <code style="font-size: 18px">x = x - 1</code>
              <br />
              <br />
              <!-- Afficher x -->
              <p>Nous pouvons afficher la valeur de x en faisant :</p>
              <code style="font-size: 18px">print(x)</code>
            </section>
          </div>
          <div class="col-2 align-self-center">
            <img
              src="View/assets/img/Foxy.png"
              alt="Logo"
              class="img-fluid rounded-circle"
              style="
                border: 2px solid #44fff6;
                background-image: url('View/assets/img/BackgroundMain.jpg');
                background-size: cover;
                background-position: center;
              "
            />
          </div>
        </div>
        <div class="row mt-5">
          <!-- Editor -->
          <div class="col-8">
            <div class="ace rounded ace-1" id="editor" style="min-height: 40vh"><?php
            if (isset ($_COOKIE['test2'])) {
                echo $_COOKIE['test2'];
            }
            else {
                echo 'x = 1 
print("La varible \'x\' :", x)

y=x+2 
print("Le résultat de\'x+2\' :", y) 

x=y*2 
print("Le résultat de \'(x+2)*2\' :",x)';
            }
            ?></div>
          </div>
          <!-- End Editor -->

          <!-- Console -->
          <div class="col-4" style="min-height: 40vh">
            <textarea
              id="console"
              readonly
              style="width: 100%; height: 60%"
              class="p-3 rounded"
            ></textarea>
            <!-- End Return Code -->

            <!-- Buttons -->
            <div
              class="row pt-5 text-center"
              style="cursor: pointer; height: 20%"
            >
              <div class="col">
                <a onclick="run_init()" class="btn">
                  <span>Exécuter</span>
                </a>
              </div>
            </div>
            <!-- End Buttons -->
          </div>
          <!-- End Console -->
        </div>
      </div>
      <!-- End First Test -->
    </div>

    <script
      src="https://ajaxorg.github.io/ace-builds/src-noconflict/ace.js"
      type="text/javascript"
      charset="utf-8"
    ></script>
    <script
      src="https://raw.githack.com/pythonpad/brython-runner/master/lib/brython-runner.bundle.js"
      type="text/javascript"
      charset="utf-8"
    ></script>
    <script src="View/src/JS/baseTest.js"></script>
  </body>
</html>
