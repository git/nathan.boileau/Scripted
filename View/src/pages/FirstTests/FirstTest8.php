<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Test de qualification</title>
    <link
      rel="stylesheet"
      href="https://stackpath.bootstrapcdn.com/bootstrap/5.0.0-alpha1/css/bootstrap.min.css"
      integrity="sha384-r4NyP46KrjDleawBgD5tp8Y7UzmLA05oM1iAEQ17CSuDqnUK2+k9luXQOfXJCJ4I"
      crossorigin="anonymous"
    />
    <script
      src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
      integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo"
      crossorigin="anonymous"
    ></script>
    <script
      src="https://stackpath.bootstrapcdn.com/bootstrap/5.0.0-alpha1/js/bootstrap.min.js"
      integrity="sha384-oesi62hOLfzrys4LxRF63OJCXdXDipiYWBnvTl9Y9/TRlw5xlKIEHpNyvvDShgf/"
      crossorigin="anonymous"
    ></script>
    <link rel="stylesheet" href="View/src/CSS/FirstTest.css" />
  </head>
  <body>
    <nav class="navbar navbar-expand-lg navbar-dark pb-5">
      <div class="container-fluid mx-0">
        <div class="nav-item nav-link">
          <a class="navbar-brand" href="index.php?action=goToHome">Accueil</a>
        </div>
        <div class="mx-auto d-flex">
          <h5
            class="m-1 text-uppercase"
            style="color: #fff; font-weight: bold; font-size: 22px"
          >
          Test de qualification
          </h5>
          <h5
          class="m-1 text-uppercase"
          style="color: #44fff6; font-weight: bold; font-size: 22px"
        > 
          8/10
        </h5>
        </div>
        <div class="nav-link">
          <a class="navbar-brand" href="index.php?action=goToNext&num=7">Précédent</a>
        </div>
      </div>
      <div class="nav-link">
          <a class="navbar-brand" href="#" style="color: gray;">Suivant</a>
        </div>
      </div>
    </nav>

    <div class="container">
      <!-- First Test -->
      <div
        class="row rounded p-3 m-3"
        style="
          background: #16222a; /* fallback for old browsers */
          background: -webkit-linear-gradient(
            to right,
            #3a6073,
            #16222a
          ); /* Chrome 10-25, Safari 5.1-6 */
          background: linear-gradient(to right, #3a6073, #16222a);
        "
      >
        <div class="row">
          <div class="col-10">
            <section
              style="background-color: #222831; min-height: 0"
              class="p-3 rounded m-0">
              <p>
                Mettons en pratique ce que tu viens d'apprendre. Crée une fonction <b style="color : violet"> condition</b>
                qui prend en argument une liste de nombre entier et un nombre x. 
                Et qui parcoure la liste en effectuant plusieurs tests :<br />
                <ul>
                  <p>Si le nombre dans la liste est égal à 1, ajoute 1 à x</p>
                  <p>Si le nombre dans la liste est différent de 2, soustrait 1 à x</p>
                  <p>Si le nombre dans la liste est inférieur à 3, multiplie x par elle-même</p>
                  <p>Si le nombre dans la liste est supérieur à 6, ajoute 4 à x</p>
                  <p>Sinon ajoute 5 à x</p>
                </ul>
              </p>
              <p>
                Pour parcourir une liste, tu peux utiliser l'instruction <b style="color:violet">for</b> comme ceci :<br />
              </p>
              <code style="font-size: 18px">
                b = 0<br />
                for i in list :<br />
                &nbsp;&nbsp;if(i == 1):<br />
                &nbsp;&nbsp;&nbsp;&nbsp;b += 1
              </code>
              <br></br>
              <p>
                Elle va parcourir la liste et mettre dans la variable "i" chaque élément de la liste. 
                Ici, elle va parcourir la liste en assignant à "i" chaque élément de la liste un par un.
                Et pour chaque élément de la liste, elle va ajouter 1 à la variable "b".
              </p>
            </section>
          </div>
          <div class="col-2 align-self-center">
            <img
              src="View/assets/img/Foxy.png"
              alt="Logo"
              class="img-fluid rounded-circle"
              style="
                border: 2px solid #44fff6;
                background-image: url('View/assets/img/BackgroundMain.jpg');
                background-size: cover;
                background-position: center;
              "
            />
          </div>
        </div>
        <div class="row mt-5">
          <!-- Editor -->
          <div class="col-8">
            <div class="ace rounded ace-1" id="editor" style="min-height: 40vh"><?php
            if (isset ($_COOKIE['test2'])) {
                echo $_COOKIE['test2'];
            }
            else {
                echo 'def condition(list,a) :';
            }
            ?></div>
          </div>
          <!-- End Editor -->

          <!-- Console -->
          <div class="col-4" style="min-height: 40vh">
            <textarea
              id="console"
              readonly
              style="width: 100%; height: 60%"
              class="p-3 rounded"
            ></textarea>
            <!-- End Return Code -->

            <!-- Buttons -->
            <div
              class="row pt-5 text-center"
              style="cursor: pointer; height: 20%"
            >
              <div class="col-6">
                <a onclick="run_init()" class="btn">
                  <span>Exécuter</span>
                </a>
              </div>
              <div class="col-6">
                <button
                  onclick="submit()"
                  class="btn"
                  data-toggle="modal"
                  data-target="#modal"
                >
                  <span>Envoyer</span>
                </button>
              </div>
            </div>
            <!-- End Buttons -->
          </div>
          <!-- End Console -->
        </div>
      </div>
      <!-- End First Test -->
    </div>

    <!-- Modal -->
    <div
      class="modal fade"
      id="modal"
      tabindex="-1"
      role="dialog"
      aria-labelledby="exampleModalCenterTitle"
      aria-hidden="true"
    >
      <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h2
              class="modal-title"
              id="exampleModalLongTitle"
              style="color: black"
            >
              Résultat
            </h2>
            <button
              type="button"
              class="close"
              data-dismiss="modal"
              aria-label="Close"
            >
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <h5 id="result" style="color: black"></h5>
          </div>
          <div class="modal-footer">
            <a href="index.php?action=goToNext&num=9" class="btn" style="display: none" id="next">
              <span>SUIVANT</span>
            </a>
          </div>
        </div>
      </div>
    </div>
    <!-- End Modal -->

    <script
      src="https://ajaxorg.github.io/ace-builds/src-noconflict/ace.js"
      type="text/javascript"
      charset="utf-8"
    ></script>
    <script
      src="https://raw.githack.com/pythonpad/brython-runner/master/lib/brython-runner.bundle.js"
      type="text/javascript"
      charset="utf-8"
    ></script>
    <script src="View/src/JS/baseTest.js"></script>
    <script src="View/src/JS/If.js"></script>
  </body>
</html>
