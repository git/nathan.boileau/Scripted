//~ Function that test the user code
          
async function submit(){
    var test = editor.getValue()+`\n
def double_elementVerif(list):
    return [i*2 for i in list]

import random as r
def testDoubleElement(x):
    l=[1,2,3,2,1]
    if(double_element(l) != [2,4,6,4,2]):
      return False
    l=[9,5,7,7,9]
    if(double_element(l) != [18,10,14,14,18]):
      return False
    l=[]
    for i in range(x):
      for j in range(r.randint(1,10)):
        l.append(r.randint(0,9))
      if(double_elementVerif(l)!=double_element(l)):
        return False
      l=[]
    return True
  
print(testDoubleElement(5))
    `;
    exec("print ('True')", "code"); 
    exec(test, "solution");
    result.innerHTML = "Test en cours...";
    await new Promise(r => setTimeout(r, 1500));
    check();
  }