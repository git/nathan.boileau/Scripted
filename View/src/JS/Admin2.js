﻿function changeCurrent(element) {
  document.querySelector(".current").classList.remove("current");
  oldText = document.querySelector(".currentText");
  oldText.classList.remove("currentText");
  oldText.classList.add("nav-button");
  element.parentElement.classList.add("current");
  element.classList.remove("nav-button");
  element.classList.add("currentText");
}
let terminal = "";
let retourCode = "";
let retourSolution = "";
function run() {
  const runner = new BrythonRunner({
    stdout: {
      write(content) {
        terminal += content;
      },
      flush() {},
    },
    stderr: {
      write(content) {
        terminal += content;
      },
      flush() {},
    },
    stdin: {
      async readline() {
        terminal += "\n";
        var userInput = prompt();
        return userInput;
      },
      flush() {},
    },
  });
  var code = getElementById("solution").value;
  runner.runCode(code);
  setTimeout(() => {
    runner.stopRunning();
  }, 10 * 1000);
}

function run_init() {
  if (terminal != "") {
    terminal = "";
  }
  run();
}

//Function that execute given code and return the result in a given element by id
function exec(code, id) {
  const runner = new BrythonRunner({
    stdout: {
      write(content) {
        if (id == "code") {
          retourCode = content;
        }
        if (id == "solution") {
          retourSolution = content;
        }
      },
      flush() {},
    },
    stderr: {
      write(content) {
        if (id == "solution") {
          retourSolution = "ERROR";
        }
        terminal += content;
      },
      flush() {},
    },
    stdin: {
      async readline() {
        terminal += "\n";
        var userInput = prompt();
        return userInput;
      },
      flush() {},
    },
  });
  runner.runCode(code);
  setTimeout(() => {
    runner.stopRunning();
  }, 10 * 1000);
}

/**
 * It checks if the code in the editor as the same result as the solution.
 */
function check() {
  if (retourSolution == "ERROR") {
    alert("Il semblerait qu'il y a une erreur de python dans le code : " + terminal);
    terminal = "";
  } else{
    alert("Aucune erreur détectée");
  }
}

async function submit(){
  // var test = document.getElementById("solution").value + "\n" +document.getElementById("test").value;
  var solution = document.getElementById("solution").value;
  exec("print ('True')", "code"); 
  // exec(test, "solution");
  exec(solution, "solution");
  var result = document.getElementById("result");
  result.innerHTML = "Test en cours...";
  await new Promise(r => setTimeout(r, 1500));
  check();
  result.innerHTML = "";
}