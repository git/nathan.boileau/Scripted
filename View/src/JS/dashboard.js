﻿function dashboard () {
    data = [];
    var xhr = new XMLHttpRequest();
    xhr.open('POST', 'http://82.165.180.114/Scripted/index.php?action=getDashboardInfo', true);
    xhr.responseType = 'text';
    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    xhr.onload = function () {
        lesInfos = JSON.parse(xhr.responseText);
        var nbJoueur = lesInfos[0].length;
        for (i = 0; i < nbJoueur; i++) {
            v1 = {}
            v1.type="line";
            v1.xValueFormatString=lesInfos[0][i];
            v1.dataPoints=[];
            var point = 0;
            v1.dataPoints.push({x:0,y:0});
            for (j = 0; j < lesInfos[1][i].length; j++) {
                point += lesInfos[1][i][j];
                v1.dataPoints.push({x:j+1,y:point});
            }
            data.push(v1);
        }
        var chart = new CanvasJS.Chart("chartContainer", {
            animationEnabled: true,
            zoomEnabled: true,
            theme: "dark2",
            title: {
                text: "Score"
            },
            axisX: {
                title: "Enigme",
                valueFormatString: "####",
                interval: 1
            },
            axisY: {
                title: "Point",
                gridThickness: 0,
                lineThickness: 1,
            },
            legend: {
                verticalAlign: "top",
                fontSize: 16,
            },
            data 
        });
        chart.render();
    }
    xhr.send(null);   
}

    // data = [];

    // if (playersPseudo[0] != null) {
    //     v1 = {}
    //     v1.type="line";
    //     v1.xValueFormatString=playersPseudo[0];
    //     v1.dataPoints=[];
    //     var xhr = new XMLHttpRequest();
    //     xhr.open('POST', 'http://82.165.180.114/Scripted/index.php?action=getPlayerScore&pseudo='. playersPseudo[0], true);
    //     xhr.responseType = 'text';
    //     xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    //     var score = xhr.onload = function () {
    //         console.log(xhr.responseText);
    //         return JSON.parse(xhr.responseText);
    //     };

    //     xhr.send(null);
    //     for (i = 0; i < score.length; i++) {
    //         v1.dataPoints.push({x:i+1,y:score[i]});
    //     }
    //     data.push(v1);
    // }

    // if (playersPseudo[1] != null) {
    //     console.log("test1");
    //     v2 = {}
    //     v2.type="line";
    //     v2.xValueFormatString=playersPseudo[1];
    //     v2.dataPoints=[];
    //     var xhr = new XMLHttpRequest();
    //     xhr.open('POST', 'http://82.165.180.114/Scripted/index.php?action=getPlayerScore&joueur='. playersPseudo[1], true);
    //     xhr.responseType = 'text';
    //     xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    //         var score = xhr.onload = function () {
    //             console.log(xhr.responseText);
    //             return JSON.parse(xhr.responseText);
    //     };
    //     xhr.send(null);
    //     for (i = 0; i < score.length; i++) {
    //         v2.dataPoints.push({x:i+1,y:score[i]});
    //     }
    //     data.push(v2);
    // }

    // if (playersPseudo[2] != null) {
    //     v3 = {}
    //     v3.type="line";
    //     v3.xValueFormatString=playersPseudo[2];
    //     v3.dataPoints=[];
    //     var xhr = new XMLHttpRequest();
    //     xhr.open('POST', 'http://82.165.180.114/Scripted/index.php?action=getPlayerScore&joueur='. playersPseudo[2], true);
    //     xhr.responseType = 'text';
    //     xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    //         var score = xhr.onload = function () {
    //             return JSON.parse(xhr.responseText);
    //     };
    //     xhr.send(null);
    //     for (i = 0; i < score.length; i++) {
    //         v3.dataPoints.push({x:i+1,y:score[i]});
    //     }
    //     data.push(v3);
    // }

    // if (playersPseudo[3] != null) {
    //     v4 = {}
    //     v4.type="line";
    //     v4.xValueFormatString=playersPseudo[3];
    //     v4.dataPoints=[];
    //     var xhr = new XMLHttpRequest();
    //     xhr.open('POST', 'http://82.165.180.114/Scripted/index.php?action=getPlayerScore&joueur='. playersPseudo[3], true);
    //     xhr.responseType = 'text';
    //     xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    //         var score = xhr.onload = function () {
    //             return JSON.parse(xhr.responseText);
    //     };
    //     xhr.send(null);
    //     for (i = 0; i < score.length; i++) {
    //         v4.dataPoints.push({x:i+1,y:score[i]});
    //     }
    //     data.push(v4);
    // }

    // data = [];

    // v4 = {}
    // v4.type="line";
    // v4.xValueFormatString='playersPseudo';
    // v4.dataPoints=[];
    // v4.dataPoints.push({x:1,y:100});
    // data.push(v4);
    // console.log(data);
// }

window.addEventListener("load", start);
function start(){
    dashboard();
    setInterval('dashboard()', 20000);

}
