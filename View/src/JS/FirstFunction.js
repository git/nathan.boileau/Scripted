//~ Function that test the user code
          
async function submit(){
    var test = editor.getValue()+`\n
import random as r

def multiVerif(a,b):
    return a*b

def multiTest(x):
    multiplication(1,1)
    for i in range(x):
        a=r.randint(0,100)
        b=r.randint(0,100)
        if(multiplication(a,b) != multiVerif(a,b)):
            return False
    return True

print(multiTest(5))
  
    `;
    exec("print('True')", "code"); 
    exec(test, "solution");
    result.innerHTML = "Test en cours...";
    await new Promise(r => setTimeout(r, 1500));
    check();
  }
  
  