﻿const observer = new IntersectionObserver((entries) => {
  entries.forEach((entry) => {
    console.log(entry);
    if (entry.isIntersecting) {
      entry.target.classList.add("show");
    } else {
      entry.target.classList.remove("show");
    }
  });
});

const hiddenElements = document.querySelectorAll(".hidden");
hiddenElements.forEach((element) => observer.observe(element));

// Increase the top position of the element when scrolling down

const fox = document.querySelector(".moving-fox");

window.addEventListener("scroll", () => {
  const scrollValue = window.scrollY - 700;
  fox.style.top = `${scrollValue / 1}px`;
  fox.style.opacity = `${window.scrollY / 800}`;
});
