function run() {
  const terminal = document.getElementById("console");
  const runner = new BrythonRunner({
    stdout: {
      write(content) {
        terminal.innerHTML += content;
        terminal.scrollTop = terminal.scrollHeight;
      },
      flush() {},
    },
    stderr: {
      write(content) {
        terminal.innerHTML += content;
        terminal.scrollTop = terminal.scrollHeight;
      },
      flush() {},
    },
    stdin: {
      async readline() {
        terminal.innerHTML += "\n";
        terminal.scrollTop = terminal.scrollHeight;
        var userInput = prompt();
        return userInput;
      },
      flush() {},
    },
  });
  var code = editor.getValue();
  runner.runCode(code);
  setTimeout(() => {
    runner.stopRunning();
  }, 10 * 1000);
}

function run_init() {
  if (document.getElementById("console") != "") {
    document.getElementById("console").innerHTML = "";
  }
  run();
}

var editor = ace.edit("editor");
editor.container.style.opacity = 0.85;
editor.setTheme("ace/theme/vibrant_ink");
editor.getSession().setMode("ace/mode/python");
editor.setFontSize("16px");
editor.setOptions({
  enableLiveAutocompletion: true,
  copyWithEmptySelection: true,
  showGutter: true,
  useWrapMode: true, // wrap text to view
  indentedSoftWrap: false,
});

//Function that execute given code and return the result in a given element by id

function exec(code, id) {
  const terminal = document.getElementById("console");
  terminal.innerHTML = "";
  const runner = new BrythonRunner({
    stdout: {
      write(content) {
        if (id == "code") {
          retourCode = content;
        }
        if (id == "solution") {
          retourSolution = content;
        }
      },
      flush() {},
    },
    stderr: {
      write(content) {
        if (id == "solution") {
          retourSolution = "ERROR";
        }
        terminal.innerHTML += content;
        terminal.scrollTop = terminal.scrollHeight;
      },
      flush() {},
    },
    stdin: {
      async readline() {
        terminal.innerHTML += "\n";
        terminal.scrollTop = terminal.scrollHeight;
        var userInput = prompt();
        return userInput;
      },
      flush() {},
    },
  });
  runner.runCode(code);
  setTimeout(() => {
    runner.stopRunning();
  }, 10 * 1000);
}

/**
 * It checks if the code in the editor as the same result as the solution.
 */
function check() {
  if (retourSolution == "ERROR") {
    result.innerHTML = "Il semblerait qu'il y a une erreur dans ton code :/";
  } else if (retourSolution == retourCode) {
    result.innerHTML = "Bien joué";
    document.getElementById("next").style.display = "flex";
  } else {
    result.innerHTML = "Mauvaise réponse";
  }
}

/**
 * If the help is displayed, hide it. Otherwise, display it.
 */
function displayHelp() {
  var help = document.getElementsByClassName("help");

  if (help[0].style.display == "block") {
    for (var i = 0; i < help.length; i++) {
      help[i].style.display = "none";
    }
    return;
  }

  for (var i = 0; i < help.length; i++) {
    help[i].style.display = "block";
  }
}

/**
 * It opens a new window with the name dashboard.html and the size of 1000x450.
 */
function dashboard() {
  window.open("index.php?action=goToDashboard", "", "width=1000, height=450");
}