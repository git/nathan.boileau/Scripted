// const url = "http://localhost/Scripted/index.php";
// const url = "http://82.165.180.114/Scripted/index.php";
const url = "http://sancy.iut.uca.fr/~lafourcade/Scripted/index.php";

function run() {
    const terminal = document.getElementById("console");
    const runner = new BrythonRunner({
      stdout: {
        write(content) {
          terminal.innerHTML += content;
          terminal.scrollTop = terminal.scrollHeight;
        },
        flush() {},
      },
      stderr: {
        write(content) {
          terminal.innerHTML += content;
          terminal.scrollTop = terminal.scrollHeight;
        },
        flush() {},
      },
      stdin: {
        async readline() {
          terminal.innerHTML += "\n";
          terminal.scrollTop = terminal.scrollHeight;
          var userInput = prompt();
          return userInput;
        },
        flush() {},
      },
    });
    var code = editor.getValue();
    runner.runCode(code);
    setTimeout(() => {
      runner.stopRunning();
    }, 10 * 1000);
  }
  
  function run_init() {
    if (document.getElementById("console") != "") {
      document.getElementById("console").innerHTML = "";
    }
    run();
  }
  
  var editor = ace.edit("editor");
  editor.container.style.opacity = 0.85;
  editor.setTheme("ace/theme/vibrant_ink");
  editor.getSession().setMode("ace/mode/python");
  editor.setFontSize("16px");
  editor.setOptions({
    enableLiveAutocompletion: true,
    copyWithEmptySelection: true,
    showGutter: true,
    useWrapMode: true, // wrap text to view
    indentedSoftWrap: false,
  });
  
  //Function that execute given code and return the result in a given element by id
  
  function exec(code, id) {
    const terminal = document.getElementById("console");
    terminal.innerHTML = "";
    const runner = new BrythonRunner({
      stdout: {
        write(content) {
          if (id == "code") {
            retourCode = content;
          }
          if (id == "solution") {
            retourSolution = content;
          }
        },
        flush() {},
      },
      stderr: {
        write(content) {
          if (id == "solution") {
            retourSolution = "ERROR";
          }
          terminal.innerHTML += content;
          terminal.scrollTop = terminal.scrollHeight;
        },
        flush() {},
      },
      stdin: {
        async readline() {
          terminal.innerHTML += "\n";
          terminal.scrollTop = terminal.scrollHeight;
          var userInput = prompt();
          return userInput;
        },
        flush() {},
      },
    });
    runner.runCode(code);
    setTimeout(() => {
      runner.stopRunning();
    }, 10 * 1000);
  }
  
  /**
   * It checks if the code in the editor as the same result as the solution.
   */
  function check() {
    if (retourSolution == "ERROR") {
      result.innerHTML = "Il semblerait qu'il y a une erreur dans ton code :/";
    } else if (retourSolution == retourCode) {
      result.innerHTML = "Bien joué";
      document.getElementById("next").style.display = "flex";
    } else {
      result.innerHTML = "Mauvaise réponse";
    }
  }
  
  
  
  /**
   * If the help is displayed, hide it. Otherwise, display it.
   */
  function displayHelp() {
    var help = document.getElementsByClassName("help");
  
    if (help[0].style.display == "block") {
      for (var i = 0; i < help.length; i++) {
        help[i].style.display = "none";
      }
      return;
    }
  
    for (var i = 0; i < help.length; i++) {
      help[i].style.display = "block";
    }
  }
  
  function saveCode() {
    var xhr = new XMLHttpRequest();
    xhr.open('POST', url + '?action=saveCodeInCookie', true);
    xhr.responseType = 'text';
    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    xhr.onload = function () {
        // console.log('saveCode'+xhr.responseText);
      };
    var url = window.location.href; // récupère l'URL de la page
    var elements = url.split("/"); // divise l'URL en un tableau de chaînes de caractères
    var lastElement = elements.pop(); // récupère le dernier élément du tableau
    if (lastElement == "index.php?action=goToTest") {
        num = 1;
    }
    else {
        var searchParams = new URLSearchParams(window.location.search);
        var num = searchParams.get('num');
    }
    xhr.send("code=" + editor.getValue() + "&num=" + num);
  }
  
  document.getElementById ('editor').addEventListener('input', saveCode);
  