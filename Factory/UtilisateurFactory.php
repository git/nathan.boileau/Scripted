<?php

class UtilisateurFactory
{
    public static function createUtilisateur(array $results) : Utilisateur{
        if ($results == null){
            return new Utilisateur("null", "null", "null", false);
        }
        foreach($results as $row)
        {
            $email = $row['email'];
            $pseudo=$row['pseudo'];
            $mdp = $row['mdp'];
            $estAdmin = $row['estAdmin'];
        }
        return new Utilisateur($email, $pseudo, $mdp, $estAdmin);
    } 
    public static function createTabUtilisateur(array $results) : array{
        $tabUtilisateur=array();
        foreach($results as $row)
        {
            $tabUtilisateur[]=new Utilisateur($row['email'], $row['pseudo'], $row['mdp'], $row['estAdmin']);
        }
        return $tabUtilisateur;
    }
}