<?php
class EnigmeFactory{
    public static function create($results) : array
    {
        $tabEnigme=array();
        foreach($results as $row)
        {
            $tabEnigme[]= new Enigme($row['id'],$row['nom'],$row['enonce'],$row['aide'],$row['rappel'],
            $row['exemple'],$row['solution'],$row['test'],$row['ordre'],$row['tempsDeResolution'],$row['points'], $row['prompt']);
        }
        return $tabEnigme;
    }
}