<?php
class PartieFactory{
    public static function createPartieMulti($newId,$tabEnigme) : Partie
    {
        if (count($tabEnigme)==0)
            return new Partie($newId,array());
        $tempsResolutionPartie=0;
        $tabIndex=range(0,count($tabEnigme));
        $randomNumber=0;
        $tabEnigmePartie = array();
        while($tempsResolutionPartie <= 30)
        {
            $randomNumber=$tabIndex[array_rand($tabIndex)];
            $tabEnigmePartie[]=$tabEnigme[$randomNumber];
            $tempsResolutionPartie+=$tabEnigme[$randomNumber]->getTempsDeResolution();
            unset($tabIndex[$randomNumber]);
        }
        $partie=new Partie($newId,$tabEnigme);
        return $partie;
    }

    public static function createPartieSolo($idMax,$resultsEnigme) : Partie
    {
        $tabEnigme=array();
        foreach($resultsEnigme as $row)
        {
            $tabEnigme[]=EnigmeFactory::create($row);
        }
        $partie=new Partie($idMax+1,$tabEnigme);
        return $partie;
    }
    public static function createPartieHistory($id,$resultsEnigme) : Partie
    {
        foreach($resultsEnigme as $row)
        {
            $tabEnigme[]=EnigmeFactory::create($row);
        }
        $partie=new Partie($id,$tabEnigme);
        return $partie;
    }
}